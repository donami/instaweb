import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter } from 'react-router-redux';

import App from './containers/App';
import reducer from './stores';
import rootSaga from './sagas';

const loggerMiddleware = createLogger();
export const history = createHistory();

const sagaMiddleware = createSagaMiddleware();

let store;

if (process.env.NODE_ENV === 'production') {

    store = createStore(
        reducer,
        applyMiddleware(
            routerMiddleware(history),
            sagaMiddleware,
        )
    );
}
else {

    store = createStore(
        reducer,
        window.devToolsExtension(),
        applyMiddleware(
            routerMiddleware(history),
            sagaMiddleware,
            loggerMiddleware,
        )
    );
}

sagaMiddleware.run(rootSaga);

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
)

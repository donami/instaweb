const REMOVE_ERROR = 'REMOVE_ERROR';

// Reducer
const errors = (state = null, action) => {

    const { payload } = action;

    if (payload && payload.error) {

        return payload.error;
    }

    if (action.type === REMOVE_ERROR) {

        return null;
    }

    return state;
}

// Action Creators
export const removeError = () => ({
    type: REMOVE_ERROR,
});

export default errors;
import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const PRODUCTS = createRequestTypes('PRODUCTS');
export const PRODUCTS_CREATE = createRequestTypes('PRODUCTS_CREATE');
export const PRODUCTS_UPDATE = createRequestTypes('PRODUCTS_UPDATE');
export const PRODUCTS_REMOVE = createRequestTypes('PRODUCTS_REMOVE');

// Reducer
const initialState = {
	ids: [],
	loaded: false,
	loading: false,
};

export default handleActions({

	[PRODUCTS.REQUEST]: state => ({
		...state,
		loaded: false,
		loading: true,
	}),

	[PRODUCTS.SUCCESS]: (state, action) => ({
		...state,
		loaded: true,
		loading: false,
		ids: action.payload.response.ids,
	}),

	[PRODUCTS.FAILURE]: state => ({
		...state,
		loaded: false,
		loading: false,
	}),

	[PRODUCTS_CREATE.SUCCESS]: (state, action) => ({
		...state,
		ids: [
			...state.ids,
			action.payload.response.result,
		],
	}),

	[PRODUCTS_REMOVE.SUCCESS]: (state, action) => ({
		...state,
		ids: state.ids.filter(id => id !== action.payload.productId),
	}),

}, initialState);

// Action Creators
export const {
    productsRequest,
    productsSuccess,
    productsFailure,
	productsCreateRequest,
	productsCreateSuccess,
	productsCreateFailure,
	productsUpdateRequest,
	productsUpdateSuccess,
	productsUpdateFailure,
	productsRemoveRequest,
	productsRemoveSuccess,
	productsRemoveFailure,
} = createActions({

    [PRODUCTS.SUCCESS]: ({ ids, entities }) => ({ response: { ids, entities } }),

    [PRODUCTS.FAILURE]: error => ({ error }),

	[PRODUCTS_CREATE.REQUEST]: product => ({ product }),

	[PRODUCTS_CREATE.SUCCESS]: response => ({ response }),

	[PRODUCTS_CREATE.FAILURE]: error => ({ error }),

	[PRODUCTS_UPDATE.REQUEST]: (productId, product) => ({ productId, product }),

	[PRODUCTS_UPDATE.SUCCESS]: response => ({ response }),

	[PRODUCTS_UPDATE.FAILURE]: error => ({ error }),

	[PRODUCTS_REMOVE.REQUEST]: productId => ({ productId }),

	[PRODUCTS_REMOVE.SUCCESS]: productId => (productId),

	[PRODUCTS_REMOVE.FAILURE]: error => ({ error }),

}, PRODUCTS.REQUEST);
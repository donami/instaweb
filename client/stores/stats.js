import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const STATS = createRequestTypes('STATS');

// Reducer
const initialState = {
	data: {},
	loaded: false,
	loading: false,
};

export default handleActions({

	[STATS.REQUEST]: state => ({
		...state,
		loaded: false,
		loading: true,
	}),

	[STATS.SUCCESS]: (state, action) => ({
		...state,
		loaded: true,
		loading: false,
		data: action.payload.data,
	}),

	[STATS.FAILURE]: state => ({
		...state,
		loaded: false,
		loading: false,
	}),

}, initialState);

// Action Creators
export const {
    statsRequest,
    statsSuccess,
    statsFailure,
} = createActions({

    [STATS.SUCCESS]: data => ({ data }),

    [STATS.FAILURE]: error => ({ error }),

}, STATS.REQUEST);
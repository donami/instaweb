import { merge, mergeWith } from 'lodash';

import {
    REMOVE_COINS,
    RECEIVE_COINS,
    DAILY_CHECKIN_CLEARED,
} from '../stores/auth';

const initialState = {
	orders: {},
    users: {},
    products: {},
    tasks: {},
    videos: {},
    logs: {},
};

const entities = (state = initialState, action) => {

    if (action.entities) {

        return merge({}, state, action.entities);
    }

    if (action.response && action.response.entities) {
        return merge({}, state, action.response.entities);
    }

    if (action.payload && action.payload.response && action.payload.response.entities) {
        return merge({}, state, action.payload.response.entities);
    }

    switch (action.type) {

        case DAILY_CHECKIN_CLEARED:

            return {
                ...state,
                users: {
                    ...state.users,
                    [action.payload.userId]: {
                        ...state.users[action.payload.userId],
                        checkins: [],
                    },
                },
            };

        case REMOVE_COINS:

            if (!action.payload.coins || !action.payload.userId || !state.users[action.payload.userId]) {

                return state;
            }

            return {
                ...state,
                users: {
                    ...state.users,
                    [action.payload.userId]: {
                        ...state.users[action.payload.userId],
                        coins: state.users[action.payload.userId].coins - action.payload.coins,
                    },
                },
            };

        case RECEIVE_COINS:

            if (!action.payload.coins || !action.payload.userId || !state.users[action.payload.userId]) {

                return state;
            }

            return {
                ...state,
                users: {
                    ...state.users,
                    [action.payload.userId]: {
                        ...state.users[action.payload.userId],
                        coins: state.users[action.payload.userId].coins + action.payload.coins,
                    },
                },
            };

        default:
            return state;
    }
};

export default entities;

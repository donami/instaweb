import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';
import {
    FOLLOW_USER,
    LIKE_IMAGE,
} from '../stores/orders';

// Action Types
export const USER_MEDIA = createRequestTypes('USER_MEDIA');
export const DAILY_CHECKIN = createRequestTypes('DAILY_CHECKIN');

export const RECEIVE_USER = 'RECEIVE_USER';
export const RECEIVE_TOKEN = 'RECEIVE_TOKEN';
export const RECEIVE_TOKEN_FAIL = 'RECEIVE_TOKEN_FAIL';
export const RECEIVE_INSTAGRAM_USER = 'RECEIVE_INSTAGRAM_USER';
export const REQUEST_ADD_COINS = 'REQUEST_ADD_COINS';
export const RECEIVE_COINS = 'RECEIVE_COINS';
export const REQUEST_REMOVE_COINS = 'REQUEST_REMOVE_COINS';
export const REMOVE_COINS = 'REMOVE_COINS';
export const RECEIVE_FOLLOWING = 'RECEIVE_FOLLOWING';
export const RECEIVE_LIKES = 'RECEIVE_LIKES';
export const DAILY_CHECKIN_CLEARED = 'DAILY_CHECKIN_CLEARED';
export const DAILY_CHECKIN_CHECK = 'DAILY_CHECKIN_CHECK';
export const LOGOUT = 'LOGOUT';

// Reducer
const initialState = {
    mediaLoaded: false,
    following: [],
    liked: [],
    media: [],
    token: null,
    userId: null,
};

export default handleActions({

    LOGOUT: (state, action) => Object.assign({}, state, initialState),

    RECEIVE_TOKEN: (state, action) => Object.assign({}, state, {
        token: action.payload.token,
    }),

    [USER_MEDIA.SUCCESS]: (state, action) => ({
        ...state,
        mediaLoaded: true,
        media: [
            ...action.payload.media,
        ],
    }),

    RECEIVE_USER: (state, action) => Object.assign({}, state, {
        userId: action.payload.userId,
    }),

    RECEIVE_FOLLOWING: (state, action) => ({
        ...state,
        following: [
            ...state.following,
            ...action.payload.following,
        ],
    }),

    RECEIVE_LIKES: (state, action) => ({
        ...state,
        liked: [
            ...state.liked,
            ...action.payload.ids,
        ],
    }),

    [FOLLOW_USER.SUCCESS]: (state, action) => {

        if (!action.payload.instagramId) {
            return state;
        }

        return {
            ...state,
            following: [
                ...state.following,
                action.payload.instagramId.toString(),
            ],
        };
    },

    [LIKE_IMAGE.SUCCESS]: (state, action) => {

        const { payload } = action;

        if (!payload.order || !payload.order.media || !payload.order.media.id) {
            return state;
        }

        return {
            ...state,
            liked: [
                ...state.liked,
                payload.order.media.id.toString(),
            ],
        };
    },

    RECEIVE_INSTAGRAM_USER: (state, action) => Object.assign({}, state, {
        instagram: action.payload.instagram,
        token: action.payload.token,
    }),

}, initialState);

// Action Creators
export const {
    receiveToken,
    receiveTokenFail,
    userMediaRequest,
    userMediaSuccess,
    userMediaFailure,
    dailyCheckinCheck,
    dailyCheckinCleared,
    dailyCheckinRequest,
    dailyCheckinSuccess,
    dailyCheckinFailure,
    receiveInstagramUser,
    receiveFollowing,
    receiveLikes,
    receiveUser,
    removeCoins,
    receiveCoins,
    logout,
} = createActions({
    RECEIVE_TOKEN: token => ({ token }),

    RECEIVE_TOKEN_FAIL: error => ({ error }),

    [USER_MEDIA.REQUEST]: token => ({ token }),

    [USER_MEDIA.SUCCESS]: media => ({ media }),

    [USER_MEDIA.FAILURE]: error => ({ error }),

    DAILY_CHECKIN_CHECK: (dates, checkins, userId) => ({ dates, checkins, userId }),

    DAILY_CHECKIN_CLEARED: userId => ({ userId }),

    [DAILY_CHECKIN.REQUEST]: (dates, userId, rewards, instagram) => ({ dates, userId, rewards, instagram }),

    [DAILY_CHECKIN.SUCCESS]: (checkins, userId, entities) => ({ checkins, userId, response: { entities } }),

    [DAILY_CHECKIN.FAILURE]: error => ({ error }),

    RECEIVE_INSTAGRAM_USER: (instagram, token) => ({ instagram, token }),

    RECEIVE_FOLLOWING: following => ({ following }),

    RECEIVE_LIKES: ids => ({ ids }),

    RECEIVE_USER: (user, userId, entities) => ({ user, userId, response: { entities } }),

    REMOVE_COINS: (coins, userId) => ({ coins, userId }),

    RECEIVE_COINS: (coins, userId) => ({ coins, userId }),

    LOGOUT: () => {
        localStorage.clear();
    },
});
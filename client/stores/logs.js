import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const LOGS = createRequestTypes('LOGS');
export const LOGS_CREATE = createRequestTypes('LOGS_CREATE');

// Reducer
const initialState = {
	ids: [],
	loaded: false,
	loading: false,
};

export default handleActions({

	[LOGS.REQUEST]: state => ({
		...state,
		loaded: false,
		loading: true,
	}),

	[LOGS.SUCCESS]: (state, action) => ({
		...state,
		loaded: true,
		loading: false,
		ids: action.payload.response.ids,
	}),

	[LOGS.FAILURE]: state => ({
		...state,
		loaded: false,
		loading: false,
	}),

	[LOGS_CREATE.SUCCESS]: (state, action) => ({
		...state,
		ids: [
			...state.ids,
			action.payload.response.ids,
		],
	}),

}, initialState);

// Action Creators
export const {
    logsCreateRequest,
    logsCreateSuccess,
    logsCreateFailure,
    logsRequest,
    logsSuccess,
    logsFailure,
} = createActions({

    [LOGS.SUCCESS]: ({ ids, entities }) => ({ response: { ids, entities } }),

    [LOGS.FAILURE]: error => ({ error }),

    [LOGS_CREATE.REQUEST]: log => ({ log }),

    [LOGS_CREATE.SUCCESS]: ({ ids, entities }) => ({ response: { ids, entities } }),

    [LOGS_CREATE.FAILURE]: error => ({ error }),

}, LOGS.REQUEST);
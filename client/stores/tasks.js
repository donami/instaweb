import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const TASKS = createRequestTypes('TASKS');
export const TASKS_CREATE = createRequestTypes('TASKS_CREATE');
export const TASKS_UPDATE = createRequestTypes('TASKS_UPDATE');
export const TASKS_REMOVE = createRequestTypes('TASKS_REMOVE');

const initialState = {
    ids: [],
    loaded: false,
    loading: false,
};

// Reducer
export default handleActions({

    [TASKS.REQUEST]: state => ({
        ...state,
        loading: true,
        loaded: false,
    }),

    [TASKS.SUCCESS]: (state, action) => ({
        ...state,
        ids: [
            ...new Set([].concat(...action.payload.response.tasks))
        ],
        loaded: true,
        loading: false,
    }),

    [TASKS.FAILURE]: (state, action) => ({
        ...state,
        loading: false,
        loaded: false,
    }),

    [TASKS_CREATE.REQUEST]: state => ({
        ...state,
        loading: true,
        loaded: false,
    }),

    [TASKS_CREATE.SUCCESS]: (state, action) => ({
        ...state,
        loading: false,
        loaded: true,
        ids: [
            ...state.ids,
            action.payload.response.result,
        ],
    }),

    [TASKS_CREATE.FAILURE]: (state, action) => ({
        ...state,
        loaded: false,
        loading: false,
    }),

    [TASKS_REMOVE.SUCCESS]: (state, action) => ({
        ...state,
        ids: state.ids.filter(id => id !== action.payload.taskId),
    }),

}, initialState);

// Action Creators
export const {
    tasksRequest,
    tasksSuccess,
    tasksFailure,
    tasksCreateRequest,
    tasksCreateSuccess,
    tasksCreateFailure,
    tasksUpdateRequest,
    tasksUpdateSuccess,
    tasksUpdateFailure,
    tasksRemoveRequest,
    tasksRemoveSuccess,
    tasksRemoveFailure,
} = createActions({

    [TASKS.SUCCESS]: ({ tasks, entities }) => ({ response: { tasks, entities } }),

    [TASKS.FAILURE]: error => ({ error }),

    [TASKS_CREATE.REQUEST]: task => ({ task }),

    [TASKS_CREATE.SUCCESS]: response => ({ response }),

    [TASKS_CREATE.FAILURE]: error => ({ error }),

    [TASKS_REMOVE.REQUEST]: taskId => ({ taskId }),

    [TASKS_REMOVE.SUCCESS]: taskId => ({ taskId }),

    [TASKS_REMOVE.FAILURE]: error => ({ error }),

    [TASKS_UPDATE.REQUEST]: (taskId, task) => ({ taskId, task }),

    [TASKS_UPDATE.SUCCESS]: (entities, tasks) => ({ response: { entities, tasks } }),

    [TASKS_UPDATE.FAILURE]: error => ({ error }),

}, TASKS.REQUEST);
import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const USERS = createRequestTypes('USERS');
export const DISABLE_USER = createRequestTypes('DISABLE_USER');
export const RESTORE_USER = createRequestTypes('RESTORE_USER');

// Reducer
const initialState = {
    loading: false,
    loaded: false,
    ids: [],
    selected: null,
};

export default handleActions({

    SELECT_USER: (state, action) => ({
        ...state,
        selected: action.payload.id,
    }),

    [USERS.REQUEST]: state => ({
        ...state,
        loading: true,
        loaded: false,
    }),

    [USERS.SUCCESS]: (state, action) => ({
        ...state,
        loading: false,
        loaded: true,
        ids: action.payload.response.ids,
    }),

    [USERS.FAILURE]: state => ({
        ...state,
        loading: false,
        loaded: false,
    }),

}, initialState);

// Action Creators
export const {
    usersSuccess,
    usersFailure,
    selectUser,
    disableUserRequest,
    disableUserSuccess,
    disableUserFailure,
    restoreUserRequest,
    restoreUserSuccess,
    restoreUserFailure,
    usersRequest,
} = createActions({

    [USERS.SUCCESS]: ({ ids, entities }) => ({ response: { ids, entities } }),

    [USERS.FAILURE]: error => ({ error }),

    SELECT_USER: id => ({ id }),

    [DISABLE_USER.REQUEST]: id => ({ id }),

    [DISABLE_USER.SUCCESS]: response => ({ response }),

    [DISABLE_USER.FAILURE]: error => ({ error }),

    [RESTORE_USER.REQUEST]: id => ({ id }),

    [RESTORE_USER.SUCCESS]: response => ({ response }),

    [RESTORE_USER.FAILURE]: error => ({ error }),

}, USERS.REQUEST);
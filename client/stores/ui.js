import { handleActions, createActions } from 'redux-actions';

// Action Types
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

// Reducer
const initialState = {
    messages: [],
};

export default handleActions({

    ADD_MESSAGE: (state, action) => ({
        ...state,
        messages: [
            ...state.messages,
            action.payload.message,
        ],
    }),

    CLOSE_MESSAGE: (state, action) => ({
        ...state,
        messages: [
            ...state.messages.slice(0, action.payload.index),
            ...state.messages.slice(action.payload.index + 1),
        ]
    }),

}, initialState);

// Action Creators
export const {
    addMessage,
    closeMessage,
} = createActions({

    ADD_MESSAGE: message => ({ message }),

    CLOSE_MESSAGE: index => ({ index }),
});
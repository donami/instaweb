import { handleActions, createActions } from 'redux-actions';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const VIDEO_FINISHED = 'VIDEO_FINISHED';
export const VIDEO_FINISHED_FAILURE = 'VIDEO_FINISHED_FAILURE';
export const VIDEO_FINISHED_SUCCESS = 'VIDEO_FINISHED_SUCCESS';

export const VIDEOS = createRequestTypes('VIDEOS');
export const VIDEOS_ADD = createRequestTypes('VIDEOS_ADD');
export const VIDEOS_REMOVE = createRequestTypes('VIDEOS_REMOVE');
export const VIDEOS_UPDATE = createRequestTypes('VIDEOS_UPDATE');

// Reducer
const initialState = {
    ids: [],
    loaded: false,
    loading: false,
};

export default handleActions({

    [VIDEOS.REQUEST]: state => ({
        ...state,
        loading: true,
        loaded: false,
    }),

    [VIDEOS.SUCCESS]: (state, action) => ({
        ...state,
        ids: [
            ...new Set([].concat(...action.payload.response.videos))
        ],
        loaded: true,
        loading: false,
    }),

    [VIDEOS.FAILURE]: state => ({
        ...state,
        loading: false,
        loaded: false,
    }),

    [VIDEOS_ADD.SUCCESS]: (state, action) => ({
        ...state,
        ids: [
            ...state.ids,
            action.payload.response.videos,
        ],
    }),

    [VIDEOS_REMOVE.SUCCESS]: (state, action) => ({
        ...state,
        ids: state.ids.filter(id => id !== action.payload.videoId),
    }),

}, initialState);

// Action Creators
export const {
    videosRequest,
    videosSuccess,
    videosFailure,
    videoFinished,
    videoFinishedSuccess,
    videoFinishedFailure,
    videosAddRequest,
    videosAddSuccess,
    videosAddFailure,
    videosRemoveRequest,
    videosRemoveSuccess,
    videosRemoveFailure,
    videosUpdateRequest,
    videosUpdateSuccess,
    videosUpdateFailure,
} = createActions({

    [VIDEOS.SUCCESS]: ({ videos, entities }) => ({ response: { videos, entities }}),

    [VIDEOS.FAILURE]: error => ({ error }),

    [VIDEO_FINISHED]: (coins, userId, type, videoId) =>
        ({ coins, userId, earnType: type, objectId: videoId }),

    [VIDEO_FINISHED_SUCCESS]: entities => ({ response: { entities } }),

    [VIDEO_FINISHED_FAILURE]: error => ({ error }),

    [VIDEOS_ADD.REQUEST]: video => ({ video }),

    [VIDEOS_ADD.SUCCESS]: (entities, videos) => ({ response: { entities, videos } }),

    [VIDEOS_ADD.FAILURE]: error => ({ error }),

    [VIDEOS_REMOVE.REQUEST]: videoId => ({ videoId }),

    [VIDEOS_REMOVE.SUCCESS]: videoId => ({ videoId }),

    [VIDEOS_REMOVE.FAILURE]: error => ({ error }),

    [VIDEOS_UPDATE.REQUEST]: (videoId, video) => ({ videoId, video }),

    [VIDEOS_UPDATE.SUCCESS]: (entities, videos) => ({ response: { entities, videos } }),

    [VIDEOS_UPDATE.FAILURE]: error => ({ error }),

}, VIDEOS.REQUEST);
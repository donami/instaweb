import { handleActions, createActions } from 'redux-actions';
import { union } from 'lodash';

import { createRequestTypes } from '../constants/helpers';

// Action Types
export const ORDERS = createRequestTypes('ORDERS');
export const ORDER_CREATE = createRequestTypes('ORDER_CREATE');
export const LIKE_IMAGE = createRequestTypes('LIKE_IMAGE');
export const FOLLOW_USER = createRequestTypes('FOLLOW_USER');
export const SKIP_ORDER = 'SKIP_ORDER';
export const SELECT_ORDER = 'SELECT_ORDER';

// Reducer
const initialState = {
	ids: [],
	skipped: [],
	selected: null,
	loaded: false,
	loading: false,
	fetchedAt: null,
};

export default handleActions({

	[ORDER_CREATE.SUCCESS]: (state, action) => ({
		...state,
		ids: [
			...state.ids,
			action.payload.id,
		],
	}),

	[ORDERS.REQUEST]: (state, action) => ({
		...state,
		loaded: false,
		loading: true,
	}),

	[ORDERS.SUCCESS]: (state, action) => ({
		...state,
		loaded: true,
		loading: false,
		fetchedAt: new Date().getTime(),
		ids: union([...state.ids, ...action.payload.response.result]),
	}),

	[ORDERS.FAILURE]: (state, action) => ({
		...state,
		loaded: false,
		loading: false,
	}),

	SELECT_ORDER: (state, action) => ({
		...state,
		selected: action.payload.id,
	}),

	SKIP_ORDER: (state, action) => {

		if (!action.payload.order || !action.payload.order._id) {

			return state;
		}

		return {
			...state,
			skipped: [
				...state.skipped,
				action.payload.order._id,
			],
		};
	},

}, initialState);

// Action Creators
export const {
    ordersRequest,
    ordersSuccess,
    ordersFailure,
    likeImageRequest,
    likeImageSuccess,
    likeImageFailure,
    followUserRequest,
    followUserSuccess,
    followUserFailure,
    selectOrder,
    skipOrder,
    orderCreateRequest,
    orderCreateSuccess,
    orderCreateFailure,
} = createActions({

    [ORDERS.SUCCESS]: response => ({ response }),

    [ORDERS.FAILURE]: error => ({ error }),

    [LIKE_IMAGE.REQUEST]: (orderId, token, userId, mediaId, reward) =>
        ({ orderId, token, userId, mediaId, reward }),

    [LIKE_IMAGE.SUCCESS]: order => ({ order }),

    [LIKE_IMAGE.FAILURE]: error => ({ error }),

    [FOLLOW_USER.REQUEST]: ({ order, auth, reward }) => ({ order, auth, reward }),

    [FOLLOW_USER.SUCCESS]: instagramId => ({ instagramId }),

    [FOLLOW_USER.FAILURE]: error => ({ error }),

    SELECT_ORDER: id => ({ id }),

    SKIP_ORDER: order => ({ order }),

    [ORDER_CREATE.REQUEST]: order => ({ order }),

    [ORDER_CREATE.SUCCESS]: ({ id, entities }) => ({ id, entities }),

    [ORDER_CREATE.FAILURE]: error => ({ error }),

}, ORDERS.REQUEST);
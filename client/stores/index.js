import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'

import auth from './auth';
import users from './users';
import ui from './ui';
import orders from './orders';
import entities from './entities';
import tasks from './tasks';
import videos from './videos';
import products from './products';
import logs from './logs';
import stats from './stats';
import errors from './errors';

const reducer = combineReducers({
    auth,
    users,
    ui,
    orders,
    entities,
    tasks,
    videos,
    products,
    logs,
    stats,
    errors,
    router: routerReducer,
});

export default reducer;

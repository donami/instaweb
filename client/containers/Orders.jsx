import React from 'react'
import { connect } from 'react-redux';
import { Header, Icon } from 'semantic-ui-react';
import { array } from 'prop-types';

import OrderList from '../components/OrderList';
import Box from '../components/Box';
import { getUserOrders } from '../selectors/orders';

class Orders extends React.Component {

    static propTypes = {
        orders: array,
    }

    static defaultProps = {
        orders: [],
    }

    render () {

        const { orders } = this.props;

        return (
            <div>
                <Header as='h2' color="teal" size="medium">
                    <Icon bordered inverted color="teal" name='shopping basket' />
                    <Header.Content>
                        Orders
                        <Header.Subheader>
                            Your order history
                        </Header.Subheader>
                    </Header.Content>
                </Header>
                <Box>
                    <h1>Orders</h1>

                    <OrderList orders={orders} />
                </Box>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        orders: getUserOrders(state),
    };
}

export default connect(mapStateToProps)(Orders);

import React from 'react';
import { string } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header, Icon } from 'semantic-ui-react';

import Box from '../components/Box';
import AdminMenu from '../components/AdminMenu';
import AdminTasks from '../components/AdminTasks';
import AdminDashboard from '../components/AdminDashboard';
import AdminUsers from '../components/AdminUsers';
import AdminVideos from '../components/AdminVideos';
import AdminProducts from '../components/AdminProducts';
import { getAuthedUserId } from '../selectors/users';

class Admin extends React.Component {

    state = {
        page: 'dashboard',
    };

    static propTypes = {
        user: string.isRequired,
    }

    changePage = page => {
        this.setState({ page });
    }

    render() {

        const { page } = this.state;

        const { user } = this.props;

        return (
            <div>
                <Header as='h2' color="green" size="medium">
                    <Icon bordered color="green" inverted name='settings' />
                    <Header.Content>
                        Admin
                        <Header.Subheader>
                            Behind the scenes...
                        </Header.Subheader>
                    </Header.Content>
                </Header>

                <Box>

                    <AdminMenu changePage={this.changePage} />

                    {page === 'dashboard' && <AdminDashboard />}
                    {page === 'videos' && <AdminVideos user={user} />}
                    {page === 'tasks' && <AdminTasks user={user} />}
                    {page === 'users' && <AdminUsers />}
                    {page === 'products' && <AdminProducts />}

                </Box>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: getAuthedUserId(state),
});

export default connect(mapStateToProps)(Admin);
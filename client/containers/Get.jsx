import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Icon, Label, Menu, Header } from 'semantic-ui-react'
import { object, func, array, bool } from 'prop-types';

import Purchase from '../components/Purchase';
import Box from '../components/Box';
import { orderCreateRequest } from '../stores/orders';
import { productsRequest } from '../stores/products';
import { getProductEntities, getProductsLoaded } from '../selectors/products';
import * as authActions from '../stores/auth';

import styles from './Get.scss';

export class Get extends React.Component {

    state = {
        selectedMedia: null,
		mediaPage: 1,
		itemsPerPage: 8,
        activeItem: 'followers',
    }

	static propTypes = {
		auth: object,
		userMediaRequest: func,
		createOrder: func,
        fetchProducts: func,
        productsLoaded: bool,
		products: array,
	}

    componentDidMount() {

		if (!this.props.auth.mediaLoaded && this.props.auth.token) {

			this.props.userMediaRequest(this.props.auth.token);
		}

        if (!this.props.productsLoaded) {
            this.props.fetchProducts();
        }
    }

    /**
     * Create order, dispatches create order action
     * @param  {String} type         Order type (followers/likes)
     * @param  {Integer} amount      Size of the order, e.g "5" followers
     * @param  {Integer} coins       Users current coins
     * @param  {Object} [media=null] If the order has an attached image
     * @return {Void}
     */
    createOrder = (type, amount, coins, media = null) => {

        this.props.createOrder({
            type,
            amount,
            coins,
            userId: this.props.auth.userId,
            instagram: this.props.auth.instagram,
            media
        });
    }

    /**
     * Menu item click handler
     * @param  {Object} e    Event
     * @param  {String} name Name of clicked menu item
     * @return {Void}
     */
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    /**
     * Select and set active image on image click
     * @param  {Object} image The selected image
     * @return {Void}
     */
    selectImage = image => {

        this.setState({
            selectedMedia: image,
        });
    }

    /**
     * Change the current page to next or previous
     * @param  {Integer} page Page number to view
     * @return {Void}
     */
	changePage = page => {

		this.setState({
			mediaPage: page,
		});
	}

    /**
     * Renders content for getting followers
     * @return {String} Followers content
     */
    _GetFollowers = () => {

		const products = this.props.products.filter(product => product.productType === 'followers') || [];

        return (
            <Purchase
				createOrder={this.createOrder}
				products={products}
				type="followers"
			/>
        );
    }

    /**
     * Renders content for getting likes
     * @return {String} Likes content
     */
    _GetLikes = () => {

		const products = this.props.products.filter(product => product.productType === 'likes') || [];

		const pagination = {
			itemsPerPage: this.state.itemsPerPage,
			currentPage: this.state.mediaPage,
			totalItems: this.props.auth.media.length,
		};

		const images = this.props.auth.media.slice((this.state.mediaPage - 1) * pagination.itemsPerPage, this.state.mediaPage * pagination.itemsPerPage);

        return (
            <Purchase
                createOrder={this.createOrder}
                type="likes"
				products={products}
                images={images}
				pagination={pagination}
				changePage={this.changePage}
                selectImage={this.selectImage}
                selectedImage={this.state.selectedMedia}
            />
        );
    }

    /**
     * Render the header on followers page
     * @return {String} Header
     */
	renderFollowersHeader = () => (
		<Header as='h2' color="blue" size="medium">
			<Icon bordered color="blue" inverted name='user circle outline' />
			<Header.Content>
				Followers
				<Header.Subheader>
					Increase your follower count today
				</Header.Subheader>
			</Header.Content>
		</Header>
	);

    /**
     * Render the headers on likes page
     * @return {String} Header
     */
	renderLikesHeader = () => (
		<Header as='h2' color="red" size="medium">
			<Icon bordered color="red" inverted name='heartbeat' />
			<Header.Content>
				Likes
				<Header.Subheader>
					Get some likes
				</Header.Subheader>
			</Header.Content>
		</Header>
	);

    render() {

        const { activeItem } = this.state;

        return (
			<div>

				{activeItem === 'followers' ?
					this.renderFollowersHeader()
					:
					this.renderLikesHeader()
				}

				<Box>
					<div className={styles.container}>

						<div className={styles.navigation}>

							<Menu vertical>
								<Menu.Item name='followers' active={activeItem === 'followers'} onClick={this.handleItemClick}>
									<Label color='teal'>
										<i className="fa fa-user-circle" aria-hidden="true"></i>
									</Label>
									Followers
								</Menu.Item>

								<Menu.Item name='likes' active={activeItem === 'likes'} onClick={this.handleItemClick}>
									<Label color='red'><i className="fa fa-heart" aria-hidden="true"></i></Label>
									Likes
								</Menu.Item>
							</Menu>

						</div>

						<div className={styles.main}>

							{activeItem === 'followers' ? this._GetFollowers() : this._GetLikes() }

						</div>

					</div>
				</Box>
			</div>
        );
    }
}

const mapStateToProps = state => {

	return {
        auth: state.auth,
		products: getProductEntities(state),
        productsLoaded: getProductsLoaded(state),
	}
}

const mapDispatchToProps = dispatch => {

	return bindActionCreators({
		userMediaRequest: authActions.userMediaRequest,
        createOrder: orderCreateRequest,
		fetchProducts: productsRequest,
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Get);

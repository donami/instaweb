import React from 'react';
import { object, func, bool, array } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Header, Icon, Button } from 'semantic-ui-react';

import { getLogsEntities, getLogsLoaded, getLogsLoading } from '../selectors/logs';
import { getUserIsAdmin } from '../selectors/users';
import { logsRequest } from '../stores/logs';
import Box from '../components/Box';
import PageLoad from '../components/PageLoad';
import LogList from '../components/LogList';

class Logs extends React.Component {

    static propTypes = {
        auth: object.isRequired,
        fetchLogs: func.isRequired,
        entities: object,
        isAdmin: bool,
        logs: array,
        loaded: bool,
        loading: bool,
    }

    static defaultProps = {
        logs: [],
    }

    componentDidMount() {

        if (!this.props.loaded) {

            this.props.fetchLogs();
        }
    }

    /**
     * Refresh logs
     * @return {Void}
     */
    refresh = () => this.props.fetchLogs();

    render() {

        if (this.props.loading) {
            return (
                <PageLoad
                    loading={this.props.loading}
                />
            )
        }

        if (!this.props.isAdmin) {
            return null;
        }

        return (
            <div>

                <div style={{ float: 'right '}}>
                    <Button onClick={this.refresh}>Refresh</Button>
                </div>

                <Header as='h2' color="red" size="medium">
                    <Icon bordered color="red" inverted name='heartbeat' />
                    <Header.Content>
                        Logs
                        <Header.Subheader>
                            Keep up with the actions
                        </Header.Subheader>
                    </Header.Content>
                </Header>

				<Box>

                    <LogList logs={this.props.logs} />

				</Box>
			</div>
        );
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    logs: getLogsEntities(state),
    isAdmin: getUserIsAdmin(state),
    loaded: getLogsLoaded(state),
    loading: getLogsLoading(state),
    entities: state.entities,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchLogs: logsRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Logs);
import React from 'react';
import { object, func, bool } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header, Icon } from 'semantic-ui-react';

import OrderDetails from '../components/OrderDetails';
import Box from '../components/Box';
import { getSelectedOrder } from '../selectors/orders';
import { getUserIsAdmin } from '../selectors/users';
import { selectOrder } from '../stores/orders';

class OrderView extends React.Component {

    static propTypes = {
        order: object,
        selectOrder: func,
        isAdmin: bool,
        match: object,
    }

    componentDidMount() {
        const orderId = this.props.match.params.id;

        this.props.selectOrder(orderId);
    }

    render() {
        const { order, isAdmin } = this.props;

        if (!order || !isAdmin) {
            return null;
        }

        return (
            <div>
                <Header as='h2' color="red" size="medium">
                    <Icon bordered color="red" inverted name='shopping basket' />
                    <Header.Content>
                        View Order
                        <Header.Subheader>
                            Order details
                        </Header.Subheader>
                    </Header.Content>
                </Header>

                <Box>
                     <OrderDetails order={order} />
                </Box>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    order: getSelectedOrder(state),
    isAdmin: getUserIsAdmin(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    selectOrder,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OrderView);
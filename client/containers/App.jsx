import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, withRouter } from 'react-router-dom';
import { Sidebar, Menu, Icon, Container, Message } from 'semantic-ui-react';
import { string, object, func, bool } from 'prop-types';

import * as authActions from '../stores/auth';
import * as uiActions from '../stores/ui';
import Home from './Home';
import Follow from './Follow';
import Like from './Like';
import Logs from './Logs';
import Get from './Get';
import Settings from './Settings';
import Earn from './Earn';
import Messages from '../components/Messages';
import Orders from './Orders';
import Footer from '../components/Footer';
import TopHeader from '../components/TopHeader';
import DailyCheckin from './DailyCheckin';
import OrderView from './OrderView';
import ProfileView from './ProfileView';
import PrivacyPolicy from './PrivacyPolicy';
import Admin from './Admin';
import TermsOfUse from './TermsOfUse';
import Authentication from '../components/Authentication';
import HandleAuth from '../components/HandleAuth';
import TopUserBar from '../components/TopUserBar';
import { removeError } from '../stores/errors';
import { ENABLED_MODULES } from '../constants/config';
import { getAuthedUser, getInstagramUser, getToken, getUserIsAdmin } from '../selectors/users';

import styles from './App.scss';
import('../global.scss');

class App extends React.Component {

    state = {
        visible: false,
    }

    static propTypes = {
        location: object.isRequired,
        token: string,
        history: object,
        currentUser: object,
        ui: object,
        instagram: object,
        logout: func,
        closeMessage: func,
        receiveToken: func,
        isAdmin: bool,
        removeError: func,
    }

    componentDidMount() {

        if (localStorage.getItem('token')) {

            this.props.receiveToken(localStorage.getItem('token'));
        }
    }

    componentDidUpdate() {

        const { location, token } = this.props;

        if (location.search.length) {

            const find = location.search.match(/\?authed/);

            if (find) {

                if (token) {

                    localStorage.setItem('token', token);
                }
            }
        }
    }

    /**
     * Toggles the visibility of the sidebar
     * @return {Void}
     */
    toggleVisibility = () => this.setState({ visible: !this.state.visible })

    /**
     * Handler for clicking on a sidebar menu item
     * @param  {Object} event _
     * @param  {Object} data  Info about the clicked item
     * @return {Void}
     */
    handleSidebarClick = (event, data) => {

        if (data.name === 'home') {

            this.props.history.push('/');
        }
        else {

            this.props.history.push(`/${data.name}`);
        }

        this.toggleVisibility();
    }

    render() {

        const { ui, errors, currentUser, instagram, token } = this.props;
        const { visible } = this.state;

        if (!token || !currentUser || !instagram) {
            return (
                <div>
                    <Authentication />

                    <Route exact path="/handle-auth" component={HandleAuth} />
                </div>
            );
        }

        return (
            <div>
                <TopHeader user={currentUser} toggleDrawer={this.toggleVisibility} profilePicture={instagram.profile_picture} />

                <Sidebar.Pushable>
                    <Sidebar as={Menu} animation='overlay' width='thin' visible={visible} icon='labeled' direction="right" vertical inverted>
                        <Menu.Item name='home' onClick={this.handleSidebarClick}>
                            <Icon name='home' />
                            Home
                        </Menu.Item>
                        <Menu.Item name='my-orders' onClick={this.handleSidebarClick}>
                            <Icon name='cart' />
                            Orders
                        </Menu.Item>

                        {/*
                            <Menu.Item name='settings' onClick={this.handleSidebarClick}>
                                <Icon name='settings' />
                                Settings
                            </Menu.Item>
                        */}

                        {this.props.isAdmin && (
                            <Menu.Item name='logs' onClick={this.handleSidebarClick}>
                                <Icon name='feed' />
                                Logs
                            </Menu.Item>
                        )}

                        {this.props.isAdmin && (
                            <Menu.Item name='admin' onClick={this.handleSidebarClick}>
                                <Icon name='settings' />
                                Admin
                            </Menu.Item>
                        )}

                        <Menu.Item name='logout' onClick={this.props.logout}>
                            <Icon name='lock' />
                            Logout
                        </Menu.Item>
                    </Sidebar>
                    <Sidebar.Pusher className={styles.pusher}>

                        <Container className={styles.outerMain}>

                            <TopUserBar user={currentUser} profilePicture={instagram.profile_picture}/>

                            <div>
                                <Messages messages={ui.messages} closeMessage={this.props.closeMessage}/>
                            </div>

                            {errors && (
                                <div>
                                    <Message negative
                                        header="Oops..."
                                        content={errors}
                                        onDismiss={this.props.removeError}
                                    />
                                </div>
                            )}

                            <div className={styles.content}>
                                <div className={styles.main}>

                                    <Route exact path="/" component={Home} />
                                    <Route exact path="/follow" component={Follow} />
                                    <Route exact path="/like" component={Like} />
                                    <Route exact path="/logs" component={Logs} />
                                    <Route exact path="/privacy-policy" component={PrivacyPolicy} />
                                    <Route exact path="/terms-of-use" component={TermsOfUse} />
                                    <Route exact path="/admin" component={Admin} />
                                    <Route path="/order/:id" component={OrderView} />
                                    <Route path="/user/:id" component={ProfileView} />

                                    {ENABLED_MODULES.earn && (<Route exact path="/earn-coins" component={Earn} />)}

                                    {ENABLED_MODULES.dailyCheckin && (<Route exact path="/daily-checkin" component={DailyCheckin} />)}
                                    <Route exact path="/my-orders" component={Orders} />
                                    <Route path="/get" component={Get} />
                                    <Route exact path="/settings" component={Settings} />

                                </div>
                            </div>

                        </Container>

                        <Footer />

                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ui: state.ui,
        errors: state.errors,
        currentUser: getAuthedUser(state),
        instagram: getInstagramUser(state),
        token: getToken(state),
        isAdmin: getUserIsAdmin(state),
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    receiveToken: authActions.receiveToken,
    addMessage: uiActions.addMessage,
    closeMessage: uiActions.closeMessage,
    removeError,
    logout: authActions.logout,
}, dispatch);

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(App));

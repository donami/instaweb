import React from 'react';
import moment from 'moment';
import { object, func } from 'prop-types';
import { Button, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getAuthedUser, getInstagramUser } from '../selectors/users';
import * as authActions from '../stores/auth';
import { DAILY_CHECKIN_REWARDS } from '../constants/config';

export class DailyCheckin extends React.Component {

    state = {
        rewards: DAILY_CHECKIN_REWARDS,
    }

    static propTypes = {
        auth: object,
        instagram: object,
        dailyCheckinRequest: func,
        dailyCheckinCheck: func,
    }

    componentDidMount() {

        const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
        const today = moment().format('YYYY-MM-DD');

        const dates = {
            today,
            yesterday,
        }

        if (this.props.auth && this.props.auth.checkins) {

            this.props.dailyCheckinCheck(dates, this.props.auth.checkins, this.props.auth._id);
        }
    }

    /**
     * Do a daily checkin
     * @return {Void}
     */
    checkin = () => {

        const { rewards } = this.state;

        const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
        const today = moment().format('YYYY-MM-DD');

        const dates = {
            today,
            yesterday,
        }

        this.props.dailyCheckinRequest(dates, this.props.auth._id, rewards, this.props.instagram);
    }

    /**
     * Render the checkin calendar
     * @return {String}
     */
    _renderCalendar = () => {

        const { auth } = this.props;
        const { rewards } = this.state;

        if (!auth || !auth.checkins) {

            return null;
        }

        const _data = [];

        const streak = auth.checkins.length;

        for (var i = 1; i <= 31; i++) {
            _data.push(i);
        }

        return (
            <div style={styles.container}>

                {_data.map((day, index) => (

                    <div key={index} style={styles.day}>

                        <Segment style={(day > streak) ? styles.dayNotChecked : styles.dayChecked}>

                            <div style={{ marginBottom: 10 }}>
                                <strong>{day}</strong>
                            </div>

                            <div>
                                <em>{rewards[index].value} {rewards[index].type}</em>
                            </div>
                        </Segment>

                    </div>

                ))}

            </div>
        );
    }

    /**
     * Render the checkin button
     * @return {String} The checkin button
     */
    _renderCheckinButton = () => {

        const { auth } = this.props;
        const today = moment().format('YYYY-MM-DD');

        if (auth && auth.checkins.indexOf(today) > -1) {

            return (
                <Button onClick={this.checkin} disabled>Checkin</Button>
            );
        }

        return (
            <Button onClick={this.checkin}>Checkin</Button>
        );
    }

    render() {

        return (
            <div>
                <div style={{ float: 'right '}}>
                    {this._renderCheckinButton()}
                </div>

                <h1>Daily Checkin</h1>

                {this._renderCalendar()}
            </div>
        );
    }
}

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexFlow: 'row wrap',
    },
    day: {
        width: '10%',
        margin: 10,
    },
    dayChecked: {
        background: '#4a5e77',
        color: '#fff',
    },
    dayNotChecked: {
    },
}

const mapStateToProps = state => ({
    auth: getAuthedUser(state),
    instagram: getInstagramUser(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    dailyCheckinRequest: authActions.dailyCheckinRequest,
    dailyCheckinCheck: authActions.dailyCheckinCheck,
}, dispatch);


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DailyCheckin);
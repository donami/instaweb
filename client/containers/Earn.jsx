import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Header, Icon, Menu } from 'semantic-ui-react';
import { func, bool, array, object } from 'prop-types';

import Box from '../components/Box';
import Tasks from '../components/Tasks';
import PageLoad from '../components/PageLoad';
import WatchVideos from '../components/WatchVideos';
import { tasksRequest } from '../stores/tasks';
import { videosRequest, videoFinished } from '../stores/videos';
import { getTaskEntities, getTasksLoading, getTasksLoaded } from '../selectors/tasks';
import { getUnwatchedVideos, getVideosLoading, getVideosLoaded } from '../selectors/videos';
import { getAuthedUser } from '../selectors/users';

class Earn extends React.Component {

    state = {
        activeItem: 'videos',
    }

    static propTypes = {
        loadTasks: func,
        loadVideos: func,
        videoFinished: func,
        tasksLoading: bool,
        videosLoading: bool,
        tasks: array,
        videos: array,
        tasksLoaded: bool,
        videosLoaded: bool,
        user: object,
    }

    componentDidMount() {
        if (!this.props.videosLoaded) {

            this.props.loadVideos();
        }

        if (!this.props.tasksLoaded) {

            this.props.loadTasks();
        }
    }

    /**
     * Handler for clicking a menu item
     * @param  {Object} e    Event
     * @param  {String} name Name of the clicked menu item
     * @return {Void}
     */
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    /**
     * Claim reward for watching a video
     * @param  {Number} coins Number of coins user should receive
     * @return {Void}
     */
    claimReward = (coins, objectId) => {

        this.props.videoFinished(coins, this.props.user._id, 'video', objectId);
    }

    /**
     * Render videos content
     * @return {String} Video content
     */
    renderVideos = () => (
        <WatchVideos claimReward={this.claimReward} videos={this.props.videos} userId={this.props.user._id} />
    )

    /**
     * Render tasks content
     * @return {String} Task content
     */
    renderTasks = () => {
        return (
            <Tasks tasks={this.props.tasks}/>
        );
    }

    render() {

        const { activeItem } = this.state;

        if (this.props.tasksLoading || this.props.videosLoading) {
            return (
                <PageLoad
                    loading={(this.props.tasksLoading || this.props.videosLoading) ? true : false}
                />
            )
        }

        return (
            <div>
                <Header as='h2' color="blue" size="medium">
                    <Icon bordered color="blue" inverted name='user circle outline' />
                    <Header.Content>
                        Earn Coins
                        <Header.Subheader>
                            Earn coins to purchase followers & likes
                        </Header.Subheader>
                    </Header.Content>
                </Header>

                <Box>

                    <Menu icon='labeled'>
                      <Menu.Item name='videos' color="blue" active={activeItem === 'videos'} onClick={this.handleItemClick}>
                        <Icon name='video' />
                        Watch Videos
                      </Menu.Item>

                      <Menu.Item name='tasks' color="red" active={activeItem === 'tasks'} onClick={this.handleItemClick}>
                        <Icon name='external' />
                        Tasks
                      </Menu.Item>

                    </Menu>

                    <div>
                        {activeItem === 'videos' && this.renderVideos()}
                        {activeItem === 'tasks' && this.renderTasks()}
                    </div>

                </Box>
            </div>
        );
    }
}

const mapStateToProps = state => {

	return {
        tasks: getTaskEntities(state),
        videos: getUnwatchedVideos(state),
        videosLoading: getVideosLoading(state),
        tasksLoading: getTasksLoading(state),
        tasksLoaded: getTasksLoaded(state),
        videosLoaded: getVideosLoaded(state),
        user: getAuthedUser(state),
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({
    loadTasks: tasksRequest,
    loadVideos: videosRequest,
    videoFinished,
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Earn);
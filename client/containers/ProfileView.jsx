import React from 'react';
import { object, func, bool, array } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header, Icon, Button } from 'semantic-ui-react';

import ProfileDetails from '../components/ProfileDetails';
import Box from '../components/Box';
import LogList from '../components/LogList';
import { getUserIsAdmin, getSelectedUser, getSelectedUserLogs } from '../selectors/users';
import { selectUser, disableUserRequest, restoreUserRequest } from '../stores/users';
import { logsRequest } from '../stores/logs';
import { getLogsLoaded } from '../selectors/logs';

class ProfileView extends React.Component {

    static propTypes = {
        user: object,
        userLogs: array,
        selectUser: func,
        disableUser: func,
        restoreUser: func,
        fetchLogs: func,
        isAdmin: bool,
        logsLoaded: bool,
        match: object,
    }

    componentDidMount() {
        const userId = this.props.match.params.id;

        this.props.selectUser(userId);

        if (!this.props.logsLoaded) {

            this.props.fetchLogs();
        }
    }

    disableUser = id => {

        this.props.disableUser(id);
    }

    _renderDisableButton() {

        const { user } = this.props;

        if (user.disabled) {
            return (
                <Button onClick={() => this.props.restoreUser(user._id)}>Restore</Button>
            );
        }

        return (
            <Button onClick={() => this.disableUser(user._id)}>Disable</Button>
        );
    }

    render() {
        const { user, isAdmin } = this.props;

        if (!user || !isAdmin) {
            return null;
        }

        return (
            <div>

                <div style={{ float: 'right' }}>
                    {this._renderDisableButton()}
                </div>

                <Header as='h2' color="blue" size="medium">
                    <Icon bordered color="blue" inverted name='user circle outline' />
                    <Header.Content>
                        View Profile
                        <Header.Subheader>
                            Profile details
                        </Header.Subheader>
                    </Header.Content>
                </Header>

                <Box>
                     <ProfileDetails user={user} />

                     <h2>User log</h2>
                     <LogList logs={this.props.userLogs} />
                </Box>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: getSelectedUser(state),
    isAdmin: getUserIsAdmin(state),
    userLogs: getSelectedUserLogs(state),
    logsLoaded: getLogsLoaded(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    selectUser,
    disableUser: disableUserRequest,
    restoreUser: restoreUserRequest,
    fetchLogs: logsRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileView);
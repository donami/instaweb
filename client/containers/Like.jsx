import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, array, object, bool } from 'prop-types';

import ImagesToLike from '../components/ImagesToLike';
import Box from '../components/Box';
import { skipOrder, ordersRequest, likeImageRequest } from '../stores/orders';
import { getOrdersToLike, getSkippedOrders, getOrdersLoading, getOrdersLoaded } from '../selectors/orders';
import PageLoad from '../components/PageLoad';
import { LIKE_MEDIA_COIN_REWARD } from '../constants/config';

class Like extends React.Component {

    static propTypes = {
        loadOrders: func,
        likeImage: func,
        skipOrder: func,
        orders: array,
        auth: object,
        skipped: array,
        loading: bool,
        loaded: bool,
    }

    componentDidMount() {

        if (!this.props.loaded) {

            this.props.loadOrders();
        }
    }

    /**
     * Dispatch like image action
     * @param  {Object} order The order of the liked image
     * @return {Void}
     */
    likeImage = order => {

		this.props.likeImage(
            order._id,
            this.props.auth.token,
            this.props.auth.userId,
            order.media.id,
            LIKE_MEDIA_COIN_REWARD
        );
	}

    /**
     * Skip / Next order
     * @param  {Object} order Order to skip
     * @return {Void}
     */
	pass = order => {

		this.props.skipOrder(order);
	}

    /**
	 * Generates a random number between min and max
	 * @param  {Integer} min Minimum number
	 * @param  {Integer} max Maximum number
	 * @return {Integer}     The random number
	 */
	randomNumber = (min, max) => {

        return Math.floor(Math.random() * (max - min + 1)) + min;
	}

    /**
	 * Selects a random order from the filtered orders
	 * @return {Object} The random order
	 */
	randomOrder = () => {

		const filtered = this.filteredOrders();

		const index = this.randomNumber(0, filtered.length - 1);

		if (!filtered[index]) {
			return null;
		}

		return filtered[index];
	}

    /**
	 * Filter the orders based on already
	 * likes and if it's been skipped
	 * @return {Array} Filtered orders
	 */
    filteredOrders = () => {

		if (!this.props.orders) {
			return [];
		}

		return this.props.orders.filter((order) => {

            if (!order.media || !order.media.id) {

                return false;
            }

            // If the order has been skipped, or the
			// user is already followed it should be ignored
			if ((this.props.auth.liked.indexOf(order.media.id.toString()) === -1)
				&& (this.props.skipped.indexOf(order._id) === -1)
			) {
				return true;
			}

			return false;
		});
	}

    render() {

        if (this.props.loading) {
            return (
                <PageLoad
                    loading={this.props.loading}
                />
            )
        }

        return (
            <Box>
                <h1>Like</h1>

                {this.filteredOrders().length ?

					<ImagesToLike
						order={this.randomOrder()}
						likeImage={this.likeImage}
						pass={this.pass}
					/>
					:
					<div>There is no more images to like at the moment.</div>
				}
            </Box>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        orders: getOrdersToLike(state),
		skipped: getSkippedOrders(state),
        loading: getOrdersLoading(state),
        loaded: getOrdersLoaded(state),
    };
}

const mapDispatchToProps = dispatch => {

	return bindActionCreators({
        loadOrders: ordersRequest,
        likeImage: likeImageRequest,
        skipOrder,
	}, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Like);

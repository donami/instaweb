import React from 'react';
import { func, object, string } from 'prop-types';
import { Modal, Header, Button } from 'semantic-ui-react';

class WatchVideo extends React.Component {

    state = {
        finished: false,
    }

    static propTypes = {
        claimReward: func.isRequired,
        video: object.isRequired,
        userId: string,
    }

    componentDidMount() {

        this.timeout = setTimeout(function () {

            this.setState({ finished: true });

        }.bind(this), this.props.video.length * 1000);
    }

    componentWillUnmount() {

        if (this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
    }

    _renderClaimReward(coins, videoId) {
        return (
            <div>
                <Button onClick={() => this.props.claimReward(coins, videoId)}>Claim reward</Button>
            </div>
        );
    }

    render() {

        const { video, userId } = this.props;

        return (
            <Modal trigger={<Button>Watch</Button>}>
                <Modal.Header>Watch Video</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Header>{video.name}</Header>

                        <div>
                            <iframe width="560" height="315" src={video.src} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
                        </div>

                        {this.state.finished ?
                            this._renderClaimReward(video.coins, video._id)
                            :
                            (<p>Watch the complete video in order to claim your coins</p>)
                        }
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

export default WatchVideo;
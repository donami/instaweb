import React from 'react';
import { object } from 'prop-types';
import { connect } from 'react-redux';
import { Header, Icon } from 'semantic-ui-react';

import { getAuthedUser } from '../selectors/users';
import Box from '../components/Box';

export class Home extends React.Component {

    static propTypes = {
        user: object,
        auth: object,
    };

    render () {
        return (
            <div>
                <Header as='h2' color="teal" size="medium">
                    <Icon bordered color="teal" inverted name='instagram' />
                    <Header.Content>
                        Welcome
                        <Header.Subheader>Lets collect some coins!</Header.Subheader>
                    </Header.Content>
                </Header>
                <Box>

                    <h4>Welcome {this.props.user.username}!</h4>

                    <div style={styles.main}>

                        <div>

                            <img src={this.props.auth.instagram.profile_picture} alt="" />

                        </div>

                        <div style={styles.content}>
                            <p>
                                Whether you have a personal Instagram account or one for your business, increasing your followers and likes will get you noticed.
                                Your account will be placed high on the platform and this will generate traffic to your profile.
                            </p>

                            <p>
                                Our goal at <strong>Followr.me</strong> is to improve your visibility on social medias by increasing your Instagram likes and followers.
                                We offer high quality followers and likes free by completing various tasks and then purchasing followers and likes for the coins received.
                            </p>

                            <p>
                                To get started click any of the links in the menu on top.
                            </p>

                            <p>
                                If you have any questions, you can get in touch with us by email.
                            </p>
                        </div>

                    </div>

                </Box>
            </div>
        );
    }
}

const styles = {
    main: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    content: {
        padding: 10,
    },
};

const mapStateToProps = state => ({
    user: getAuthedUser(state),
    auth: state.auth,
});

export default connect(mapStateToProps)(Home);

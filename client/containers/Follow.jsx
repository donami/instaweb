import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { func, object, array, bool } from 'prop-types';

import { getOrdersToFollow, getSkippedOrders, getOrdersLoading, getOrdersLoaded } from '../selectors/orders';
import { skipOrder, followUserRequest, ordersRequest } from '../stores/orders';
import UsersToFollow from '../components/UsersToFollow';
import Box from '../components/Box';
import PageLoad from '../components/PageLoad';
import { FOLLOW_USER_COIN_REWARD } from '../constants/config';

class Follow extends React.Component {

	static propTypes = {
		loadOrders: func,
		followUserRequest: func,
		skipOrder: func,
		auth: object,
		orders: array,
		skipped: array,
		loading: bool,
		loaded: bool,
	}

	componentDidMount() {

		if (!this.props.loaded) {

			this.props.loadOrders();
		}
	}

	/**
	 * Follow user, dispatches action
	 * @param  {Object} order The order
	 * @return {Void}
	 */
	followUser = order => {

		const data = {
			order,
			auth: this.props.auth,
			reward: FOLLOW_USER_COIN_REWARD,
		}

		this.props.followUserRequest(data);
	}

	/**
	 * Skip order
	 * @param  {Object} order The order to skip
	 * @return {Void}
	 */
	pass = order => {

		this.props.skipOrder(order);
	}

	/**
	 * Generates a random number between min and max
	 * @param  {Integer} min Minimum number
	 * @param  {Integer} max Maximum number
	 * @return {Integer}     The random number
	 */
	randomNumber = (min, max) => {

		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	/**
	 * Selects a random order from the filtered orders
	 * @return {Object} The random order
	 */
	randomOrder = () => {

		const filtered = this.filteredOrders();

		const index = this.randomNumber(0, filtered.length - 1);

		if (!filtered[index]) {
			return null;
		}

		return filtered[index];
	}

	/**
	 * Filter the orders based on already
	 * following and if it's been skipped
	 * @return {Array} Filtered orders
	 */
	filteredOrders = () => {

		if (!this.props.orders) {
			return [];
		}

		return this.props.orders.filter((order) => {

			// If the order has been skipped, or the
			// user is already followed it should be ignored
			if ((this.props.auth.following.indexOf(order.instagram.id.toString()) === -1)
				&& (this.props.skipped.indexOf(order._id) === -1)
			) {
				return true;
			}

			return false;
		});
	}

    render() {

		if (this.props.loading) {
			return (
				<PageLoad
					loading={this.props.loading}
				/>
			)
		}

        return (
            <Box>
				<h1>Follow</h1>

				{this.filteredOrders().length ?

					<UsersToFollow
						order={this.randomOrder()}
						followUser={this.followUser}
						pass={this.pass}
					/>
					:
					<div>There is no more users to follow at the moment.</div>
				}
            </Box>
        )
    }
}

const mapStateToProps = state => {

	return {
		auth: state.auth,
		orders: getOrdersToFollow(state),
		skipped: getSkippedOrders(state),
		loading: getOrdersLoading(state),
		loaded: getOrdersLoaded(state),
	}
}

const mapDispatchToProps = dispatch => {

	return bindActionCreators({
		skipOrder,
		followUserRequest,
		loadOrders: ordersRequest,
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Follow);

import React from 'react';
import { array, func } from 'prop-types';
import { List, Button } from 'semantic-ui-react';

import AdminProductsForm from './AdminProductsForm';

const AdminProductsList = ({ products, remove, update }) => {

    return (
        <List>
            {products.map(product => (

                <List.Item key={product._id}>

                    <List.Content floated="right">

                        <AdminProductsForm updateProduct={update} product={product} />

                        <Button
                            onClick={() => remove(product._id)}
                            circular
                            size="mini"
                            color="red"
                            icon="trash"
                            title="Remove"
                        />

                    </List.Content>

                    <List.Content>

                        {product.name}

                    </List.Content>

                </List.Item>
            ))}
        </List>
    );
};

AdminProductsList.propTypes = {
    products: array.isRequired,
    remove: func,
    update: func,
};

export default AdminProductsList;

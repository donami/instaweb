import React from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { object, func } from 'prop-types';

import LikeFollowWrap from './LikeFollowWrap';
import Coins from './Coins';

const UsersToFollow = ({ pass, followUser, order }) => {

	if (!order || !order.instagram) {
		return null;
	}

	return (
		<LikeFollowWrap>

			<div style={styles.left}>

				<div style={{ height: 80 }}>
					<img src={order.instagram.profile_picture} alt="Profile picture" style={styles.profilePicture} />
				</div>

				<Coins coins={4} />

			</div>

			<div style={styles.right}>

				<h4 style={styles.username}>{order.instagram.username}</h4>

				<div>
					<Button onClick={() => followUser(order)} color="instagram">
						<Icon name='user circle' /> Follow
					</Button>
					<Button onClick={() => pass(order)}>Next</Button>
				</div>

			</div>

		</LikeFollowWrap>
	);
}

UsersToFollow.propTypes = {
	pass: func.isRequired,
	followUser: func.isRequired,
	order: object.isRequired,
};

const styles = {
	profilePicture: {
		width: 64,
		display: 'block',
	},
	left: {
		float: 'left',
	},
	right: {
		marginLeft: 40,
		float: 'left',
	},
	username: {
		padding: 0,
		margin: 0,
		height: 80,
	}
};

export default UsersToFollow;

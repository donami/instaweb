import React from 'react';
import { object } from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';

const LogItem = ({ log, user }) => {

    if (!user) {

        return null;
    }

    /**
     * Render log message for "order created" event
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderOrderCreated = log => (
        <div>
            <Link to={`/user/${user._id}`}>{user.username}</Link> created an <Link to={`/order/${log.order}`}>order</Link>.
        </div>
    );

    /**
     * Render log message for "user created" event
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderUserCreated = log => (
        <div>
            <Link to={`/user/${user._id}`}>{user.username}</Link> created a user.
        </div>
    );

    /**
     * Render log message for "video created" event
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderVideoAdded = log => (
        <div>
            <Link to={`/user/${user._id}`}>{user.username}</Link> added a video.
        </div>
    );

    /**
     * Render log message for "video updated" event
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderVideoUpdated = log => (
        <div>
            <Link to={`/user/${user._id}`}>{user.username}</Link> updated a video.
        </div>
    );

    /**
     * Render log message for "task created" event
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderTaskCreated = log => (
        <div>
            <Link to={`/user/${user._id}`}>{user.username}</Link> added a task.
        </div>
    );

    /**
     * Render log message for "task updated" event
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderTaskUpdated = log => (
        <div>
            <Link to={`/user/${user._id}`}>{user.username}</Link> updated a task.
        </div>
    );

    /**
     * Render message based on the log's eventType
     * @param  {Object} log The log event
     * @return {String}     Log message
     */
    const _renderMessage = (log) => {

        switch (log.eventType) {

            case 'order_create':

                return _renderOrderCreated(log);

            case 'user_create':

                return _renderUserCreated(log);

            case 'video_created':

                return _renderVideoAdded(log);

            case 'video_updated':

                return _renderVideoUpdated(log);

            case 'task_created':

                return _renderTaskCreated(log);

            case 'task_updated':

                return _renderTaskUpdated(log);

            default:
                return (
                    <div>{log.message}</div>
                );
        }
    }

    return (
        <div style={styles.logItem}>

            <div style={styles.logColumn}>
                {_renderMessage(log)}
            </div>

            <div style={styles.logColumn}>
                {moment(log.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
            </div>

        </div>
    );
}

LogItem.propTypes = {
    log: object.isRequired,
    user: object,
};

const styles = {
    logItem: {
        display: 'flex',
        justifyContent: 'space-around',
    },
    logColumn: {
        flex: '0 0 50%',
    },
};

export default LogItem;

import React from 'react';
import { array, func } from 'prop-types';
import { List, Button, Confirm } from 'semantic-ui-react';

import AdminTasksEdit from './AdminTasksEdit';

class AdminTasksList extends React.Component {

    state = {
        confirmDeleteOpen: false,
        selectedId: null,
    }

    static propTypes = {
        tasks: array,
        removeTask: func.isRequired,
        updateTask: func.isRequired,
    }

    /**
     * Display confirmation popup for removing task
     * @param  {String} taskId ID of task to remove
     * @return {Void}
     */
    _confirmDelete = taskId => this.setState({ confirmDeleteOpen: true, selectedId: taskId });

    /**
     * Event handler, fires when removal is confirmed
     * @return {Void}
     */
    _handleConfirmDelete = () => {

        this.setState({ confirmDeleteOpen: false });

        if (this.state.selectedId) {
            this.props.removeTask(this.state.selectedId);
        }
    }

    /**
     * Event handler, fires when removal is cancelled
     * @return {Void}
     */
    _handleCancelDelete = () => this.setState({ confirmDeleteOpen: false, selectedId: null });

    render() {

        const { tasks } = this.props;

        if (!tasks.length) {

            return <div>No tasks added yet</div>;
        }

        return (

            <List divided verticalAlign="middle">

                <Confirm
                    open={this.state.confirmDeleteOpen}
                    header='Do you really want to remove this task?'
                    onCancel={this._handleCancelDelete}
                    onConfirm={this._handleConfirmDelete}
                />

                {tasks.map(task => (

                    <List.Item key={task._id}>

                        <List.Content verticalAlign="middle" floated="right">

                            <Button circular
                                icon="trash outline"
                                size="mini"
                                color="red"
                                onClick={() => this._confirmDelete(task._id)}
                            />

                        <AdminTasksEdit task={task} updateTask={this.props.updateTask} />

                        </List.Content>

                        <List.Content verticalAlign="middle">

                            {task.name}

                        </List.Content>

                    </List.Item>
                ))}
            </List>
        );
    }
}

export default AdminTasksList;
import React from 'react';
import { object, func } from 'prop-types';
import { Modal, Header, Button, Form } from 'semantic-ui-react';

class AdminVideosEdit extends React.Component {

    state = {
        values: {
            name: '',
            src: '',
            coins: '',
            length: '',
            user: '',
        },
        submitted: false,
    };

    static propTypes = {
        video: object.isRequired,
        updateVideo: func.isRequired,
    }

    componentDidMount() {

        const { video } = this.props;

        this.setState({
            values: {
                name: video.name,
                src: video.src,
                coins: video.coins,
                length: video.length,
                user: video.user,
            },
        });
    }

    /**
     * Event handler for change event on form inputs
     * @param  {String} propertyName The property to change
     * @return {Void}
     */
    _handleChange = propertyName => event => {

        const newValues = {
            ...this.state.values,
            [propertyName]: event.target.value,
        };

        this.setState({ values: newValues });
    }

    /**
     * Event handler for form submit
     * @return {Void}
     */
    _handleSubmit = () => {

        this.props.updateVideo(this.props.video._id, this.state.values);
    }

    render() {

        return (
            <Modal trigger={<Button circular size="mini" color="blue" icon="edit" />}>
                <Modal.Header>Watch Video</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Header>Title</Header>

                        <Form onSubmit={this._handleSubmit}>
                            <Form.Field>
                                <label>Name</label>
                                <input placeholder='Name' onChange={this._handleChange('name')} value={this.state.values.name} />
                            </Form.Field>
                            <Form.Field>
                                <label>Source</label>
                                <input placeholder='Source' onChange={this._handleChange('src')} value={this.state.values.src} />
                            </Form.Field>
                            <Form.Field>
                                <label>Coins</label>
                                <input placeholder='Coins' onChange={this._handleChange('coins')} value={this.state.values.coins} />
                            </Form.Field>
                            <Form.Field>
                                <label>Length</label>
                                <input placeholder='Video length in seconds' onChange={this._handleChange('length')} value={this.state.values.length} />
                            </Form.Field>

                            <Button type='submit'>Save</Button>
                        </Form>

                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

export default AdminVideosEdit;

import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

function withPagination(WrappedComponent, selectData, options = null) {

    return class PaginationWrapper extends React.Component {

        state = {
            data: [],
            itemsPerPage: 1,
            page: 1,
            totalItems: 0,
            pageCount: 0,
        }

        componentDidMount() {

            if (options) {
                this.setState({
                    ...options,
                });
            }
        }

        componentWillReceiveProps(nextProps) {

            const selectedData = selectData(nextProps);

            if (selectedData && selectedData.length) {
                this.setState({
                    data: selectedData,
                    totalItems: selectedData.length,
                    pageCount: this.getPageCount(selectedData.length, this.state.itemsPerPage),
                });
            }
        }

        /**
         * Filter the data
         * @param  {Array} data Items
         * @return {Array}      Filtered items
         */
        filterData(data) {
            return data.slice((this.state.page - 1) * this.state.itemsPerPage, this.state.page * this.state.itemsPerPage);
        }

        /**
         * Set the page
         * @param {Number} page Page to set
         */
        setPage = page => {

            this.setState({ page });
        }

        /**
         * Get number of pages
         * @param  {Number} totalItems   Number of items
         * @param  {Number} itemsPerPage Items per page
         * @return {Number}              Number of pages
         */
        getPageCount(totalItems, itemsPerPage) {
            return Math.ceil(totalItems / itemsPerPage);
        }

        /**
         * Create page range to display
         * @param  {Number} activePage Current active page
         * @return {Array}             Pages
         */
        createPageRange = activePage => {

            const { pageCount } = this.state;

            // If the page count is less than maximum
            // pages, simply return the range
            if (pageCount <= 7) {

                return _.range(1, pageCount + 1);
            }

            let first, middle, end;
            if (activePage === 1) {
                first = [];
                middle = _.range(1, 4);
                end = [pageCount];
            }
            else if (activePage === pageCount) {
                first = [1];
                middle = _.range(activePage - 1, activePage + 2);
                end = [pageCount];
            }
            else {
                first = [1];
                middle = _.range(activePage - 1, activePage + 2);
                end = [pageCount];
            }

            // Remove values outside of page count
            _.remove(middle, value => value > pageCount);

            let full = _.union([
                ...first,
                ...middle,
                ...end,
            ]);

            let prevValue;
            let range = full.reduce((acc, value, index) => {

                if (prevValue && prevValue + 1 !== value) {

                    acc.push('...');
                }

                if (prevValue !== '...') {
                    prevValue = value;
                }

                acc.push(value);

                return acc;
            }, []);

            if (range.length < 7 && range.indexOf('...') > -1) {

                let index = range.indexOf('...');

                while (range.length < 7) {

                    range.splice(index, 0, range[index - 1] + 1);
                    index++;
                }
            }

            return range;
        }

        /**
         * Render
         * @return {String}
         */
        render() {

            const { itemsPerPage, page, pageCount, totalItems, data } = this.state;

            const injectedProps = {
                data: this.filterData(data),
                pagination: {
                    itemsPerPage,
                    page,
                    totalItems,
                    changePage: this.setPage,
                    pageCount,
                    pageRange: this.createPageRange(this.state.page),
                },
            };

            return (
                <WrappedComponent {...this.props} {...injectedProps} />
            )
        }
    }
}

export default withPagination;
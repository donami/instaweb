import React from 'react';
import PropTypes from 'prop-types';

import styles from './Box.scss';

const Box = ({ children }) => {

    return (
        <div className={styles.box}>
            {children}
        </div>
    );
}

Box.propTypes = {
    children: PropTypes.node,
}

export default Box;

import React from 'react';
import { object, func } from 'prop-types';
import { Modal, Header, Button, Form } from 'semantic-ui-react';

class AdminTasksEdit extends React.Component {

    state = {
        values: {
            name: '',
            url: '',
            coins: '',
            user: '',
        },
        submitted: false,
    };

    static propTypes = {
        task: object.isRequired,
        updateTask: func.isRequired,
    }

    componentDidMount() {

        const { task } = this.props;

        this.setState({
            values: {
                name: task.name,
                url: task.url,
                coins: task.coins,
                user: task.user,
            },
        });
    }

    /**
     * Event handler for change event on form inputs
     * @param  {String} propertyName The property to change
     * @return {Void}
     */
    _handleChange = propertyName => event => {

        const newValues = {
            ...this.state.values,
            [propertyName]: event.target.value,
        };

        this.setState({ values: newValues });
    }

    /**
     * Event handler for form submit
     * @return {Void}
     */
    _handleSubmit = () => {

        this.props.updateTask(this.props.task._id, this.state.values);
    }

    render() {

        return (
            <Modal trigger={<Button circular size="mini" color="blue" icon="edit" />}>
                <Modal.Header>Watch Task</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Header>Title</Header>

                        <Form onSubmit={this._handleSubmit}>
                            <Form.Field>
                                <label>Name</label>
                                <input placeholder='Name' onChange={this._handleChange('name')} value={this.state.values.name} />
                            </Form.Field>
                            <Form.Field>
                                <label>Url</label>
                                <input placeholder='Url' onChange={this._handleChange('url')} value={this.state.values.url} />
                            </Form.Field>
                            <Form.Field>
                                <label>Coins</label>
                                <input placeholder='Coins' onChange={this._handleChange('coins')} value={this.state.values.coins} />
                            </Form.Field>

                            <Button type='submit'>Save</Button>
                        </Form>

                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

export default AdminTasksEdit;

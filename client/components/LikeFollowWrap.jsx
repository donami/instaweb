import React from 'react';
import { any } from 'prop-types';

import styles from './LikeFollowWrap.scss';

const LikeFollowWrap = ({ children }) => {
    return (
		<div className={styles.container}>
			<div className={styles.inner}>

                {children}

			</div>
		</div>
	);
}

LikeFollowWrap.propTypes = {
    children: any,
};

export default LikeFollowWrap;

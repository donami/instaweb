import React from 'react';
import { array, func, object } from 'prop-types';
import { List, Button, Confirm } from 'semantic-ui-react';

import AdminVideosEdit from './AdminVideosEdit';
import Pagination from './Pagination';
import withPagination from './PaginationWrapper';

class AdminVideosList extends React.Component {

    state = {
        confirmDeleteOpen: false,
        selectedId: null,
    }

    static propTypes = {
        data: array,
        removeVideo: func.isRequired,
        updateVideo: func.isRequired,
        pagination: object.isRequired,
    }

    /**
     * Display confirmation popup for removing video
     * @param  {String} videoId ID of video to remove
     * @return {Void}
     */
    _confirmDelete = videoId => this.setState({ confirmDeleteOpen: true, selectedId: videoId });

    /**
     * Event handler, fires when removal is confirmed
     * @return {Void}
     */
    _handleConfirmDelete = () => {

        this.setState({ confirmDeleteOpen: false });

        if (this.state.selectedId) {
            this.props.removeVideo(this.state.selectedId);
        }
    }

    /**
     * Event handler, fires when removal is cancelled
     * @return {Void}
     */
    _handleCancelDelete = () => this.setState({ confirmDeleteOpen: false, selectedId: null });

    render() {

        const { data } = this.props;

        if (!data.length) {

            return <div>No videos added yet</div>;
        }

        return (

            <div>
                <List divided verticalAlign="middle">

                    <Confirm
                        open={this.state.confirmDeleteOpen}
                        header='Do you really want to remove this video?'
                        onCancel={this._handleCancelDelete}
                        onConfirm={this._handleConfirmDelete}
                    />

                    {data.map(video => (

                        <List.Item key={video._id}>

                            <List.Content verticalAlign="middle" floated="right">

                                <Button circular
                                    icon="trash outline"
                                    size="mini"
                                    color="red"
                                    onClick={() => this._confirmDelete(video._id)}
                                />

                                <AdminVideosEdit video={video} updateVideo={this.props.updateVideo} />

                            </List.Content>

                            <List.Content verticalAlign="middle">

                                {video.name}

                            </List.Content>

                        </List.Item>
                    ))}
                </List>

                <Pagination {...this.props.pagination} />

            </div>
        );
    }
}

export default withPagination(
    AdminVideosList,
    props => props.videos,
    {
        itemsPerPage: 10,
    },
);
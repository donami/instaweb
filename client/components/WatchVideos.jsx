import React from 'react';
import PropTypes from 'prop-types';
import { Image, List, Modal, Header } from 'semantic-ui-react';

import WatchVideo from '../containers/WatchVideo';

const WatchVideos = ({ videos, claimReward, userId }) => {

    if (!videos.length) {

        return (
            <div>There are no more videos to watch. Come back later.</div>
        );
    }

    return (
        <List divided verticalAlign='middle'>
            {videos.map(video => (

                <List.Item key={video._id}>
                    <List.Content floated='right'>

                        <WatchVideo claimReward={claimReward} video={video} userId={userId} />

                    </List.Content>
                    <Image avatar src='/assets/video.png' />
                    <List.Content>
                        {video.name}
                    </List.Content>
                </List.Item>
            ))}
        </List>
    );
};

WatchVideos.propTypes = {
    videos: PropTypes.array.isRequired,
    claimReward: PropTypes.func.isRequired,
    userId: PropTypes.string.isRequired,
};

WatchVideos.defaultProps = {
    videos: [],
};

export default WatchVideos;

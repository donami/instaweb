import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, bool, func } from 'prop-types';
import { Button, Header, Divider } from 'semantic-ui-react';

import AdminProductsList from './AdminProductsList';
import AdminProductsForm from './AdminProductsForm';
import { getProductEntities, getProductsLoaded } from '../selectors/products';
import * as productActions from '../stores/products';

export class AdminProducts extends React.Component {

    state = {
        typeFilter: null,
    }

    static propTypes = {
        products: array,
        loaded: bool,
        loadProducts: func,
        addProduct: func,
        updateProduct: func,
        removeProduct: func,
    }

    static defaultProps = {
        products: [],
        loaded: false,
    }

    componentDidMount() {

        if (!this.props.loaded && this.props.loadProducts) {

            this.props.loadProducts();
        }
    }

    /**
     * Remove product event handler
     * @param  {String} productId ID of product to remove
     * @return {Void}
     */
    _removeProduct = productId => {

        this.props.removeProduct(productId);
    }

    /**
     * Add product event handler
     * @param {Object} product Product to add
     */
    _addProduct = product => {

        this.props.addProduct(product);
    }

    /**
     * Update product event handler
     * @param  {String} productId ID of product to update
     * @param  {Object} data      New product data
     * @return {Void}
     */
    _updateProduct = (productId, data) => {

        this.props.updateProduct(productId, data);
    }

    /**
     * Filter product by productType
     * @param  {String} type Filter by, "followers/likes"
     * @return {Void}
     */
    _filter = type => {

        if (this.state.typeFilter === type) {

            this.setState({ typeFilter: null });
        }
        else {
            this.setState({ typeFilter: type });
        }
    }

    /**
     * Sort product by product type
     * @param  {Object} product Product
     * @param  {Object} other   Product to compare to
     * @return {Boolean}
     */
    _sort = (product, other) => product.productType > other.productType;

    /**
     * Returns filtered and sorted products
     * @return {Array} Array of filtered products
     */
    _filteredProducts() {

        if (!this.state.typeFilter) {
            return this.props.products.sort(this._sort);
        }

        return this.props.products
            .filter(product => product.productType === this.state.typeFilter)
            .sort(this._sort);
    }

    /**
     * Render filter button, based on current state
     * @param  {String} type Filter name, "Followers/Likes"
     * @return {String} Rendered button
     */
    _renderFilterButton(type) {

        const props = {
            active: this.state.typeFilter === type ? true : false,
            size: 'tiny',
            color: 'blue',
            content: type,
        };

        return (<Button {...props} onClick={() => this._filter(type.toLowerCase())} />);
    }

    render() {
        return (
            <div>

                <div style={{ float: 'right' }}>
                    <AdminProductsForm createProduct={this._addProduct} />
                </div>


                <div>
                    <Header size="medium">Filter by type</Header>

                    {this._renderFilterButton('Followers')}
                    {this._renderFilterButton('Likes')}
                </div>

                <Divider />

                <Header size="large">Products</Header>

                <AdminProductsList
                    products={this._filteredProducts()}
                    remove={this._removeProduct}
                    update={this._updateProduct}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    products: getProductEntities(state),
    loaded: getProductsLoaded(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadProducts: productActions.productsRequest,
    addProduct: productActions.productsCreateRequest,
    updateProduct: productActions.productsUpdateRequest,
    removeProduct: productActions.productsRemoveRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdminProducts);
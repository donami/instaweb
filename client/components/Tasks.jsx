import React from 'react';
import PropTypes from 'prop-types';

const Tasks = ({ tasks }) => {

    if (!tasks.length) {
        return <div>No tasks found, or you have completed the existing ones.</div>
    }

    return (
        <div>
            <ul>
                {tasks.map(task => {
                    return (
                        <li key={task._id}>{task.name}</li>
                    );
                })}
            </ul>
        </div>
    );
}

Tasks.propTypes = {
    tasks: PropTypes.array.isRequired,
};

Tasks.defaultProps = {
    tasks: [],
};

export default Tasks;

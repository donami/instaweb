import React from 'react';
import { func, string, array } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AdminVideosAdd from './AdminVideosAdd';
import AdminVideosList from './AdminVideosList';
import { getVideoEntities, getVideosLoaded } from '../selectors/videos';
import { videosRequest, videosRemoveRequest, videosUpdateRequest, videosAddRequest } from '../stores/videos';

class AdminVideos extends React.Component {

    static propTypes = {
        addVideo: func.isRequired,
        removeVideo: func.isRequired,
        updateVideo: func.isRequired,
        loadVideos: func.isRequired,
        user: string.isRequired,
        videos: array,
    }

    static defaultProps = {
        videos: [],
    }

    componentDidMount() {
        if (!this.props.videosLoaded) {

            this.props.loadVideos();
        }
    }

    /**
     * Remove video, dispatches remove video action
     * @param  {String} videoId ID of video to remove
     * @return {Void}
     */
    _removeVideo = videoId => {
        this.props.removeVideo(videoId);
    }

    /**
     * Update video, dispatches update video action
     * @param  {String} videoId ID of video to update
     * @param  {Object} video   The video with updated data
     * @return {Void}
     */
    _updateVideo = (videoId, video) => {

        this.props.updateVideo(videoId, video);
    }

    /**
     * Add video, dispatches add video action
     * @param {Object} video Video data to add
     * @return {Void}
     */
    _addVideo = video => {

        this.props.addVideo(video);
    }

    render() {

        const { user, videos } = this.props;

        return (
            <div>
                <h2>Add new video</h2>

                <AdminVideosAdd addVideo={this._addVideo} user={user} />

                <h2>Videos</h2>

                <AdminVideosList
                    videos={videos}
                    removeVideo={this._removeVideo}
                    updateVideo={this._updateVideo}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    videos: getVideoEntities(state),
    loaded: getVideosLoaded(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadVideos: videosRequest,
    addVideo: videosAddRequest,
    removeVideo: videosRemoveRequest,
    updateVideo: videosUpdateRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdminVideos);

import React from 'react';
import { func, object, number } from 'prop-types';
import { Message } from 'semantic-ui-react';

const Toast = ({ message, closeMessage, index }) => {

    let type = {};

    switch (message.type) {

        case 'error':
            type.negative = true;

            break;

        case 'success':
            type.positive = true;

            break;

        case 'info':
            type.info = true;

            break;
    }

    return (
        <Message key={index} {...type} onDismiss={() => closeMessage(index)}>
            {message.text}
        </Message>
    );
}

Toast.propTypes = {
    message: object.isRequired,
    closeMessage: func.isRequired,
    index: number,
};

export default Toast;

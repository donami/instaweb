import React from 'react';
import { object } from 'prop-types';

const ProfileDetails = ({ user }) => {

    return (
        <div>
            <h2>View Profile</h2>

            <table>
                <tbody>
                    <tr>
                        <td><strong>Username:</strong></td>
                        <td>{user.username}</td>
                    </tr>
                    <tr>
                        <td><strong>Current checkins:</strong></td>
                        <td>{user.checkins.length}</td>
                    </tr>
                    <tr>
                        <td><strong>Coins:</strong></td>
                        <td>{user.coins}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

ProfileDetails.propTypes = {
    user: object,
};

export default ProfileDetails;

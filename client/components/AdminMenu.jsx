import React from 'react';
import PropTypes from 'prop-types';

import styles from './AdminMenu.scss';

const AdminMenu = ({ changePage }) => {
    return (

        <div className={styles.container}>
            <div className={styles.item} onClick={() => changePage('dashboard')}>
                Dashboard
            </div>

            <div className={styles.item} onClick={() => changePage('users')}>
                Users
            </div>

            <div className={styles.item} onClick={() => changePage('videos')}>
                Videos
            </div>

            <div className={styles.item} onClick={() => changePage('tasks')}>
                Tasks
            </div>
            <div className={styles.item} onClick={() => changePage('products')}>
                Products
            </div>
        </div>
    );
};

export default AdminMenu;

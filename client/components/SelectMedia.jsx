import React from 'react';
import { func, object, array } from 'prop-types';
import { Button } from 'semantic-ui-react';

const SelectMedia = ({ selectImage, selectedImage, pagination, changePage, images }) => {

	const { totalItems, itemsPerPage, currentPage } = pagination;

	const _renderNextButton = () => {

		if (currentPage > Math.floor(totalItems / itemsPerPage)) {

			return (
				<Button disabled>Next page</Button>
			);
		}

		return (
			<Button onClick={() => changePage(currentPage + 1)}>Next page</Button>
		);
	}

	const _renderPrevButton = () => {

		if (totalItems <= itemsPerPage || currentPage === 1) {

			return (
				<Button disabled>Prev page</Button>
			);
		}

		return (
			<Button onClick={() => changePage(currentPage - 1)}>Prev page</Button>
		);
	}

	return (
		<div>
			<div style={styles.imageList}>
				{images.map(item => (

					<div key={item.id}
						style={Object.assign({},
							styles.image,
							(selectedImage && selectedImage.id === item.id) && styles.selected
						)}
					>

						<img src={item.images.thumbnail.url} alt="Image" onClick={() => selectImage(item)}/>

					</div>
				))}
			</div>

			<div>
				{_renderPrevButton()}
				{_renderNextButton()}
			</div>
		</div>
	);
}

SelectMedia.propTypes = {
	selectImage: func.isRequired,
	selectedImage: object,
	pagination: object,
	changePage: func.isRequired,
	images: array,
};

const styles = {
    imageList: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    image: {
        padding: 10,
        border: '1px solid transparent',
        cursor: 'pointer',
    },
    selected: {
        border: '1px solid blue',
    },
};

export default SelectMedia;

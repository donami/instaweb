import React from 'react';
import { Link } from 'react-router-dom';
import { object, func, string } from 'prop-types';
import { Header, Image, Icon } from 'semantic-ui-react'
import classnames from 'classnames';

import { ENABLED_MODULES } from '../constants/config';
import styles from './TopHeader.scss';

const TopHeader = ({ user, profilePicture, toggleDrawer }) => {

    if (!user) {

		return false;
	}

    return (
        <div className={styles.header}>
            <div className={styles.content}>

                <div className={styles.node}>
                    <ul className={styles.menu}>
                        <li><Link to="/follow">Follow</Link></li>
                        <li><Link to="/like">Like</Link></li>
                        <li><Link to="/get">Get</Link></li>
                        { ENABLED_MODULES.earn && (<li><Link to="/earn-coins">Earn Coins</Link></li>)}
                        { ENABLED_MODULES.dailyCheckin && (<li><Link to="/daily-checkin">Daily Checkin</Link></li>)}
                    </ul>
                </div>

                <div className={styles.node} style={{ textAlign: 'center', paddingTop: 15 }}>
                    <Link to="/" style={{ textDecoration: 'none' }}>
                        <img src="/assets/logo.png" alt="Logo" style={{ height: 48 }}/>
                    </Link>
                </div>

                <div className={classnames(styles.node, styles.right)}>
                    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>

                        <div>

                            <Header as='h5' className={styles.user}>
                                <Image circular src={profilePicture} />
                                {' '} {user.username}
                            </Header>

                        </div>

                        <div className={styles.drawerIcon} onClick={() => toggleDrawer()}>
                            <Icon name="list" fitted size="big" />
                        </div>

                    </div>
                </div>

            </div>
        </div>
    )
}

TopHeader.propTypes = {
    user: object,
    profilePicture: string,
    toggleDrawer: func,
};

export default TopHeader;

import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Footer.scss';

const Footer = () => {
    return (
        <footer className={styles.container}>
            <div className={styles.copyright}>
                © Copyright 2017, Followr.me - All rights reserved
            </div>

            <div>
                <Link to="/privacy-policy">Privacy Policy</Link> &nbsp;
                <Link to="/terms-of-use">Terms of Use</Link>
            </div>
        </footer>
    );
}

export default Footer;

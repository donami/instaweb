import React from 'react';
import { bool } from 'prop-types';
import { HashLoader } from 'react-spinners';

const Loader = ({ loading }) => {
    return (
        <HashLoader
           color={'#00BBB0'}
           loading={loading}
         />
    )
}

Loader.propTypes = {
    loading: bool.isRequired,
};

export default Loader;

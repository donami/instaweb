import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';

const Coins = ({ coins, reverse }) => {

	if (reverse) {
		return (
			<span>
				<Label color="yellow">
					<Icon name='gg' /> {coins}
				</Label>
			</span>
		);
	}

	return (
		<span>
			<Label color="yellow">
				<Icon name='gg' /> {coins}
			</Label>
		</span>
	);
};

Coins.defaultProps = {
	coins: 0,
	reverse: false,
};

Coins.propTypes = {
	coins: PropTypes.number,
	reverse: PropTypes.bool,
};

export default Coins;

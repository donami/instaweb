import React from 'react';
import { func, string, array } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AdminTasksAdd from './AdminTasksAdd';
import AdminTasksList from './AdminTasksList';
import { getTaskEntities, getTasksLoaded } from '../selectors/tasks';
import { tasksRequest, tasksRemoveRequest, tasksUpdateRequest, tasksCreateRequest } from '../stores/tasks';

class AdminTasks extends React.Component {

    static propTypes = {
        addTask: func.isRequired,
        removeTask: func.isRequired,
        updateTask: func.isRequired,
        loadTasks: func.isRequired,
        user: string.isRequired,
        tasks: array,
    }

    static defaultProps = {
        tasks: [],
    }

    componentDidMount() {
        if (!this.props.tasksLoaded) {

            this.props.loadTasks();
        }
    }

    /**
     * Remove task, dispatches remove task action
     * @param  {String} taskId ID of task to remove
     * @return {Void}
     */
    _removeTask = taskId => {
        this.props.removeTask(taskId);
    }

    /**
     * Update task, dispatches update task action
     * @param  {String} taskId ID of task to update
     * @param  {Object} task   The task with updated data
     * @return {Void}
     */
    _updateTask = (taskId, task) => {

        this.props.updateTask(taskId, task);
    }

    /**
     * Add task, dispatches add task action
     * @param {Object} task Task data to add
     * @return {Void}
     */
    _addTask = task => {

        this.props.addTask(task);
    }

    render() {

        const { user, tasks } = this.props;

        return (
            <div>
                <h2>Add new task</h2>

                <AdminTasksAdd addTask={this._addTask} user={user} />

                <h2>Tasks</h2>

                <AdminTasksList
                    tasks={tasks}
                    removeTask={this._removeTask}
                    updateTask={this._updateTask}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tasks: getTaskEntities(state),
    loaded: getTasksLoaded(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadTasks: tasksRequest,
    addTask: tasksCreateRequest,
    removeTask: tasksRemoveRequest,
    updateTask: tasksUpdateRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdminTasks);

import React from 'react';
import { array, object, func } from 'prop-types';
import { Button } from 'semantic-ui-react';

import LogItem from './LogItem';

class LogList extends React.Component {

    state = {
        itemsPerPage: 10,
        totalItems: 0,
        page: 1,
    }

    componentDidMount() {
        this.setState({
            totalItems: this.props.logs.length,
        });
    }

    /**
     * Previous page
     * @return {Void}
     */
    prevPage = () => this.setState({ page: this.state.page - 1 });

    /**
     * Next page
     * @return {Void}
     */
    nextPage = () => this.setState({ page: this.state.page + 1 });

    /**
     * Get the logs to display depending on current page
     * @return {Array} Logs to display
     */
    filteredLogs() {
        return this.props.logs.slice(
            (this.state.page - 1) * this.state.itemsPerPage,
             this.state.page * this.state.itemsPerPage
        );
    }

    _renderPrevButton = () => {

		if (this.state.totalItems <= this.state.itemsPerPage || this.state.page === 1) {

			return (
				<Button disabled>Previous</Button>
			);
		}

		return (
			<Button onClick={this.prevPage}>Previous</Button>
		);
	}

    _renderNextButton = () => {

        if (this.state.page > Math.floor(this.state.totalItems / this.state.itemsPerPage)) {

            return (
                <Button disabled>Next</Button>
            );
        }

        return (
            <Button onClick={this.nextPage}>Next</Button>
        );
    }

    render() {

        return (
            <div>

                {this.filteredLogs().map(log => (
                    <LogItem
                        log={log}
                        user={log.user}
                        key={log._id}
                    />
                ))}

                <div style={styles.buttons}>
                    {this._renderPrevButton()}

                    {this._renderNextButton()}
                </div>

            </div>
        );
    }
}

const styles = {
    buttons: {
        marginTop: 20,
    },
};

LogList.propTypes = {
    logs: array,
    nextPage: func,
    prevPage: func,
};

export default LogList;

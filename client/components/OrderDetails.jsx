import React from 'react';
import { object } from 'prop-types';

const OrderDetails = ({ order }) => {
    return (
        <div>
            <h2>View Order</h2>

            <table>
                <tbody>
                    <tr>
                        <td><strong>Amount:</strong></td>
                        <td>{order.completed} / {order.amount} {order.orderType}</td>
                    </tr>
                    <tr>
                        <td><strong>Fulfilled:</strong></td>
                        <td>{order.fulfilled ? 'Yes' : 'No'}</td>
                    </tr>
                    <tr>
                        <td><strong>Instagram user:</strong></td>
                        <td>{order.instagram.username}</td>
                    </tr>
                    <tr>
                        <td><strong>Created at:</strong></td>
                        <td>{order.createdAt}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

OrderDetails.propTypes = {
    order: object,
};

export default OrderDetails;

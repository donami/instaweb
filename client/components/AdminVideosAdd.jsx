import React from 'react';
import { func, string } from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

class AdminVideosAdd extends React.Component {

    state = {
        values: {
            name: '',
            src: '',
            coins: '',
            length: '',
            user: '',
        },
        submitted: false,
    };

    static propTypes = {
        addVideo: func.isRequired,
        user: string.isRequired,
    }

    componentDidMount() {
        this.setState({
            values: {
                ...this.state.values,
                user: this.props.user,
            },
        });
    }

    /**
     * Event handler for change event on form inputs
     * @param  {String} propertyName The property to change
     * @return {Void}
     */
    _handleChange = propertyName => event => {

        const newValues = {
            ...this.state.values,
            [propertyName]: event.target.value,
        };

        this.setState({ values: newValues });
    }

    /**
     * Event handler for form submit
     * @return {Void}
     */
    _handleSubmit = () => {

        this.props.addVideo(this.state.values);
        this.setState({ submitted: true });
    }

    /**
     * Reset form to initial state
     */
    _reset = () => {

        this.setState({
            values: {
                name: '',
                src: '',
                coins: '',
                length: '',
                user: this.props.user,
            },
            submitted: false,
        });
    }

    /**
     * Render component
     * @return {String}
     */
    render() {

        if (this.state.submitted) {

            return (
                <div>
                    <p>Video added, click <span style={{ cursor: 'pointer' }} onClick={this._reset}>here</span> to add another one.</p>
                </div>
            );
        }

        return (
            <Form onSubmit={this._handleSubmit}>
                <Form.Field>
                    <label>Name</label>
                    <input placeholder='Name' onChange={this._handleChange('name')} value={this.state.values.name} />
                </Form.Field>
                <Form.Field>
                    <label>Source</label>
                    <input placeholder='Source' onChange={this._handleChange('src')} value={this.state.values.src} />
                </Form.Field>
                <Form.Field>
                    <label>Coins</label>
                    <input placeholder='Coins' onChange={this._handleChange('coins')} value={this.state.values.coins} />
                </Form.Field>
                <Form.Field>
                    <label>Length</label>
                    <input placeholder='Video length in seconds' onChange={this._handleChange('length')} value={this.state.values.length} />
                </Form.Field>

                <Button type='submit'>Add</Button>
            </Form>
        );
    }
}

export default AdminVideosAdd;

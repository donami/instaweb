import React from 'react';
import { array, func } from 'prop-types';

import Toast from './Toast';

const Messages = ({ messages, closeMessage }) => {
    return (
        <div style={styles.container}>

            {messages.map((message, index) => (

                <Toast message={message} index={index} key={index} closeMessage={closeMessage} />

            ))}

        </div>
    );
}

Messages.propTypes = {
    messages: array,
    closeMessage: func,
};

Messages.defaultProps = {
    messages: [],
};

const styles = {
    container: {
        margin: '20px 0',
    },
};

export default Messages;

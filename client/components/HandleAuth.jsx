import React from 'react'
import { connect } from 'react-redux';
import { object, func } from 'prop-types';

import * as authActions from '../stores/auth';

class HandleAuth extends React.Component {

    static propTypes = {
        location: object,
        onSuccessfullAuth: func,
    };

    componentDidMount() {

        const hash = this.props.location.hash;

        const token = hash.match(/token=([a-zA-Z0-9\.]*)/)[1];

        if (token) {

            this.props.onSuccessfullAuth(token);
        }
    }

    render() {
        return null;
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSuccessfullAuth: token => {

            dispatch(authActions.receiveToken(token));
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HandleAuth);

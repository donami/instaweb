import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Icon } from 'semantic-ui-react'

import Coins from './Coins';
import SelectMedia from './SelectMedia';

import styles from './Purchase.scss';

const Purchase = ({ createOrder, type, products, pagination, changePage, selectImage, selectedImage, images }) => {

      let typeIcon = '';

      if (type === 'followers') {
            typeIcon = <Icon name="user circle" size="large" />
      }
      else if (type === 'likes') {
            typeIcon = <Icon name="heart" size="large" />
      }

      let renderImages;

      if (type === 'likes') {
            renderImages = (
                  <SelectMedia
                        createOrder={createOrder}
                        type={type}
                        changePage={changePage}
                        pagination={pagination}
                        selectImage={selectImage}
                        selectedImage={selectedImage}
                        images={images}
                  />
            );
      }

      return (
          <div>
            <h1>Get {type}</h1>

            {renderImages}

            {(type === 'likes' && selectedImage === null) ?
                  null
                  :
                  <Segment.Group raised>

                        {products.map((product, index) => (
                           <Segment key={index}>
                                 <span className={styles.follower}>
                                       {typeIcon} {product.displayName}
                                 </span>

                                 <span className={styles.right}>
                                       <Coins coins={product.coins} reverse={true} />

                                       <div className={styles.basket} onClick={() => createOrder(type, product.amount, product.coins, selectedImage)}>
                                             <Icon name="shopping basket" size="large" />
                                       </div>
                                 </span>
                           </Segment>
                        ))}

                  </Segment.Group>
            }

          </div>
      );
};

Purchase.propTypes = {
    createOrder: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    products: PropTypes.array.isRequired,
    pagination: PropTypes.object,
    changePage: PropTypes.func,
    selectImage: PropTypes.func,
    selectedImage: PropTypes.object,
    images: PropTypes.array,
};

export default Purchase;

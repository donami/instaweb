import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object } from 'prop-types';
import { Button } from 'semantic-ui-react';

import AdminStats from './AdminStats';
import * as statsActions from '../stores/stats';

class AdminDashboard extends React.Component {

    static propTypes = {
        stats: object,
        loadStats: func,
    }

    componentDidMount() {

        if (!this.props.stats.loaded) {

            this.props.loadStats();
        }
    }

    /**
     * Refresh stats
     * @return {Void}
     */
    _refresh = () => this.props.loadStats();

    render() {

        const btnProps = {
            loading: this.props.stats.loading,
        }

        return (
            <div>
                <Button circular icon="refresh" {...btnProps} color="blue" onClick={this._refresh} style={{ float: 'right' }} />

                <h2>Admin home</h2>

                <AdminStats {...this.props.stats.data}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    stats: state.stats,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadStats: statsActions.statsRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdminDashboard);

import React from 'react';
import { array, object, func } from 'prop-types';
import { Link } from 'react-router-dom';
import { Button, Input, List } from 'semantic-ui-react';

import withPagination from './PaginationWrapper';
import Pagination from './Pagination';

class AdminUsersList extends React.Component {

    static propTypes = {
        data: array,
        search: func.isRequired,
        pagination: object.isRequired,
    }

    _filter = (e, { value }) => {

        this.props.search(value);
        this.props.pagination.changePage(1);
    }

    render() {

        return (
            <div>

                <Input type="text" onChange={this._filter} placeholder="Filter users.." />

                <h3>List of Users</h3>

                {!this.props.data.length && <div>No users found.</div>}

                <List divided verticalAlign="middle">

                    {this.props.data.map(user => (

                        <List.Item key={user._id}>
                            <List.Content>

                                <Link to={`/user/${user._id}`}>{user.username}</Link>

                            </List.Content>
                        </List.Item>
                    ))}

                </List>

                <Pagination {...this.props.pagination} />

            </div>
        );
    }
}

const styles = {
    buttons: {
        marginTop: 20,
    },
};

export default withPagination(
    AdminUsersList,
    props => props.users,
    {
        itemsPerPage: 10,
    },
);
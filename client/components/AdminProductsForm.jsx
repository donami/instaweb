import React from 'react';
import { object, func } from 'prop-types';
import { Modal, Header, Button, Form, Select } from 'semantic-ui-react';

class AdminProductsForm extends React.Component {

    state = {
        values: {
            name: '',
            displayName: '',
            productType: '',
            amount: 0,
            coins: 0,
        },
        submitted: false,
    };

    static propTypes = {
        product: object,
        updateProduct: func,
        createProduct: func,
    }

    componentDidMount() {

        const { product } = this.props;

        if (product) {

            this.setState({
                values: {
                    name: product.name,
                    displayName: product.displayName,
                    coins: product.coins,
                    productType: product.productType,
                    amount: product.amount,
                },
            });
        }
    }

    /**
     * Event handler for change event on form inputs
     * @param  {String} propertyName The property to change
     * @return {Void}
     */
    _handleChange = propertyName => event => {

        const newValues = {
            ...this.state.values,
            [propertyName]: event.target.value,
        };

        this.setState({ values: newValues });
    }

    /**
     * Event handler for form submit
     * @return {Void}
     */
    _handleSubmit = () => {

        if (this.props.product) {

            this.props.updateProduct(this.props.product._id, this.state.values);
        }
        else {
            this.props.createProduct(this.state.values);
        }
    }

    _renderTrigger = () => {
        if (this.props.product) {
            return (<Button circular size="mini" color="blue" icon="edit" />);
        }
        else {
            return (<Button>Create product</Button>);
        }
    }

    _handleSelectChange = (e, { value }) => {

        this.setState({
            values: {
                ...this.state.values,
                productType: value,
            },
        });
    }

    render() {

        const createProductBtn = (<Button icon="add" labelPosition="left" content="Create product" color="green" />);
        const editProductBtn = (<Button circular size="mini" color="blue" icon="edit" />);

        const modalProps = {
            trigger: this.props.product ? editProductBtn : createProductBtn,
        }

        const productTypes = [
            { key: 'followers', text: 'Followers', value: 'followers' },
            { key: 'likes', text: 'Likes', value: 'likes' },
        ];

        return (
            <Modal {...modalProps}>
                <Modal.Header>Product</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Header>Title</Header>

                        <Form onSubmit={this._handleSubmit}>
                            <Form.Field>
                                <label>Name</label>
                                <input placeholder='Name' onChange={this._handleChange('name')} value={this.state.values.name} />
                            </Form.Field>
                            <Form.Field>
                                <label>Display name</label>
                                <input placeholder='Display name' onChange={this._handleChange('displayName')} value={this.state.values.displayName} />
                            </Form.Field>
                            <Form.Field>
                                <label>Amount</label>
                                <input placeholder='Amount' type="number" onChange={this._handleChange('amount')} value={this.state.values.amount} />
                            </Form.Field>
                            <Form.Field>
                                <label>Product type</label>
                                <Select placeholder='Product type' options={productTypes} value={this.props.product ? this.props.product.productType : null} onChange={this._handleSelectChange} />
                            </Form.Field>
                            <Form.Field>
                                <label>Coins</label>
                                <input placeholder='Coins' type="number" onChange={this._handleChange('coins')} value={this.state.values.coins} />
                            </Form.Field>

                            <Button type='submit'>Save</Button>
                        </Form>

                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

export default AdminProductsForm;

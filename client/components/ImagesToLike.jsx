import React from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { object, func } from 'prop-types';

import LikeFollowWrap from './LikeFollowWrap';
import Coins from './Coins';

const ImagesToLike = ({ pass, likeImage, order }) => {

	if (!order || !order.media) {

		return null;
	}

	return (
		<LikeFollowWrap>

			<div style={{
				marginBottom: 10,
				marginRight: 10,
				overflow: 'hidden'
			}}>
				<div style={{ float: 'right' }} >

					<Coins coins={5} />

				</div>
			</div>

			<img src={order.media.images.thumbnail.url} alt="Image" />

			<div>
				<Button onClick={() => likeImage(order)} color="red">
					<Icon name='heart' /> Like
				</Button>
				<Button onClick={() => pass(order)}>Next</Button>
			</div>

		</LikeFollowWrap>
	);
}

ImagesToLike.propTypes = {
	pass: func.isRequired,
	likeImage: func.isRequired,
	order: object.isRequired,
};

export default ImagesToLike;

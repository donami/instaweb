import React from 'react';
import PropTypes from 'prop-types';

import Loader from './Loader';

const PageLoad = ({ loading }) => {
    return (
        <div style={styles.container}>
            <Loader
                loading={loading}
            />
        </div>
    )
}

PageLoad.propTypes = {
    loading: PropTypes.bool.isRequired,
};

const styles = {
    container: {
        position: 'absolute',
        left: '50%',
        transform: 'translate(-50%, -50%)',
    },
}

export default PageLoad;

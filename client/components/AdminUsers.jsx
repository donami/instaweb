import React from 'react';
import { array, bool } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AdminUsersList from './AdminUsersList';
import { usersRequest } from '../stores/users';
import { getUsersEntities, getUsersLoaded } from '../selectors/users';

class AdminUsers extends React.Component {

    state = {
        query: null,
    }

    static propTypes = {
        users: array,
        loaded: bool,
    }

    static defaultProps = {
        users: [],
        loaded: false,
    }

    componentDidMount() {

        if (!this.props.loaded) {

            this.props.fetchUsers();
        }
    }

    /**
     * Refresh users from backend
     * @return {Void}
     */
    _refresh = () => this.props.fetchUsers();

    /**
     * Set state query property
     * @param  {String} query Filter by string
     * @return {Void}
     */
    search = query => this.setState({ query });

    /**
     * Filter users by query
     * @param  {String} query Filter by
     * @return {Array}        Filtered users
     */
    _filter = query => this.props.users
        .filter(user => user.username.search(query) > -1);

    render() {

        const filtered = this.state.query ? this._filter(this.state.query) : this.props.users;

        return (
            <div>
                <h2>Manage users</h2>

                <AdminUsersList users={filtered} search={this.search} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    users: getUsersEntities(state),
    loaded: getUsersLoaded(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchUsers: usersRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdminUsers);

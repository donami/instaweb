import React from 'react';
import PropTypes from 'prop-types';

import Coins from './Coins';

const TopUserBar = ({ user }) => {

	if (!user) {

		return false;
	}

	return (
		<div style={styles.container}>

			<div style={styles.coins}>

				<Coins coins={user.coins} />

			</div>

		</div>
	);
}

TopUserBar.propTypes = {
	user: PropTypes.object,
};

const styles = {
	container: {
		overflow: 'hidden',
		width: '100%',
		padding: '10px 0'
	},
	coins: {
		float: 'right',
	},
};

export default TopUserBar;

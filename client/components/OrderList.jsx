import React from 'react';
import { array } from 'prop-types';
import moment from 'moment';
import { Icon } from 'semantic-ui-react';

const OrderList = ({ orders }) => {

    if (!orders.length) {
        return <div>You have not made any orders, yet.</div>
    }

    return (
        <ul style={styles.container}>

            <li style={styles.orderItem}>

                <div style={styles.orderColumn}>
                    <strong>Type</strong>
                </div>

                <div style={styles.orderColumn}>
                    <strong>Amount</strong>
                </div>

                <div style={styles.orderColumn}>
                    <strong>Status</strong>
                </div>

                <div style={styles.orderColumn}>
                    <strong>Date created</strong>
                </div>

            </li>

            {orders.map(order => (

                <li style={styles.orderItem} key={order._id}>

                    <div style={styles.orderColumn}>

                        {order.fulfilled ?
                            <Icon name="check circle" />
                            :
                            <Icon name="circle outline" />
                        }

                        {{
                            'followers': (
                                <span>Followers</span>
                            ),
                            'likes': (
                                <span>Likes</span>
                            )
                        }[order.orderType]}
                    </div>

                    <div style={styles.orderColumn}>
                        {order.amount}
                    </div>

                    <div style={styles.orderColumn}>
                        {order.fulfilled ?
                            'Completed' :
                            `In progress (${order.completed}/${order.amount})`
                        }
                    </div>

                    <div style={styles.orderColumn}>
                        {moment(order.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
                    </div>

                </li>

            ))}

        </ul>
    );
}

OrderList.propTypes = {
    orders: array,
};

OrderList.defaultProps = {
    array: [],
};

const styles = {
    container: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
    },
    orderItem: {
        padding: 10,
        display: 'flex',
        justifyContent: 'space-around',
    },
    orderColumn: {
        flex: '0 0 25%',
    },
};

export default OrderList;

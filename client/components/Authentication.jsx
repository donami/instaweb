import React from 'react';
import { Container, Button, Segment, Icon, Grid, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import { CLIENT_ID,
    // CLIENT_SECRET,
    REDIRECT_URI
 } from '../services/instagram';

import styles from './Authentication.scss';

class Authentication extends React.Component {

    state = {
        windowHeight: 0,
    }

    componentDidMount() {

        this._updateWindowSize();
        window.addEventListener('resize', this._updateWindowSize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._updateWindowSize);
    }

    _updateWindowSize = () => {
        this.setState({ windowHeight: window.innerHeight });
    }

    _handleClick() {

        window.location = `https://api.instagram.com/oauth/authorize/?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=token&scope=follower_list+relationships+likes+public_content`;
    }

    render() {
        return (
            <div className={styles.container} style={{ minHeight: this.state.windowHeight }}>

                <div className={styles.topBar}>
                    <div style={{ position: 'absolute', right: 15, top: 15 }}>
                        <Button color="instagram" onClick={() => this._handleClick()}>
                            <Icon name="instagram" /> Sign in using Instagram
                        </Button>
                    </div>

                    <Link to="/">
                        <img src="/assets/logo.png" alt="Logo" style={{ height: 48 }}/>
                    </Link>
                </div>

                <Container>

                    <img src="http://instagram.com/accounts/logout/" width="0" height="0" />

                    <Grid columns="equal">

                        <Grid.Row>

                            <Grid.Column width={10}>

                                <h2 className={styles.welcomeText}>Start getting Instagram followers &amp; likes now. It is easy!</h2>

                            </Grid.Column>

                            <Grid.Column>

                                <Segment color="blue" style={{ color: '#777', marginBottom: 50 }}>

                                    <Header as='h3'>
                                        <Icon name='question' color="blue" />
                                        <Header.Content>
                                            How it works
                                        </Header.Content>
                                    </Header>

                                    <p>
                                        Our goal at <strong>Followr.me</strong> is to improve your visibility on social medias by increasing your Instagram likes and followers.
                                        We offer high quality followers and likes free by completing various tasks and then purchasing followers and likes for the coins received.
                                    </p>
                                </Segment>

                                <Segment color="green" style={{ color: '#777' }}>

                                    <Header as='h3'>
                                        <Icon name='info' color="green" />
                                        <Header.Content>
                                            What is Followr.me?
                                        </Header.Content>
                                    </Header>

                                    <p>
                                        Followr.me offer you a free way of earning Instagram followers & likes, by liking media uploaded by other users of the <em>Followr.me</em> community.
                                    </p>
                                </Segment>

                            </Grid.Column>
                        </Grid.Row>
                    </Grid>

                </Container>

            </div>
        );
    }
}

export default Authentication;

import React from 'react';
import { object } from 'prop-types';
import { Statistic, Image, Icon, Divider } from 'semantic-ui-react';

const AdminStats = ({ orders, users }) => {
    return (
        <div>

            {orders && (
                <Statistic.Group widths="three">
                    <Statistic>
                        <Statistic.Value>{orders.incomplete}</Statistic.Value>
                        <Statistic.Label>Incomplete orders</Statistic.Label>
                    </Statistic>

                    <Statistic>
                        <Statistic.Value>{orders.complete}</Statistic.Value>
                        <Statistic.Label>Fullfilled orders</Statistic.Label>
                    </Statistic>

                    <Statistic>
                        <Statistic.Value>
                            <Icon name="shopping cart" />
                            {orders.total}
                        </Statistic.Value>
                        <Statistic.Label>Orders placed</Statistic.Label>
                    </Statistic>
                </Statistic.Group>
            )}

            <Divider />

            {users && (
                <Statistic.Group widths="one">
                    <Statistic size='huge'>
                        <Statistic.Label>Users</Statistic.Label>
                        <Statistic.Value>{users.total}</Statistic.Value>
                    </Statistic>
                </Statistic.Group>
            )}
        </div>
    );
}

AdminStats.propTypes = {
    orders: object,
    users: object,
};

export default AdminStats;

import React from 'react';
import { number, func, array } from 'prop-types';
import { times } from 'lodash';
import { Button } from 'semantic-ui-react';

import styles from './Pagination.scss';

class Pagination extends React.Component {

    static propTypes = {
        pageCount: number.isRequired,
        changePage: func.isRequired,
        itemsPerPage: number.isRequired,
        page: number.isRequired,
        pageRange: array.isRequired,
    }

    /**
     * Event handler for next page
     * @return {Void}
     */
    _handleNextClick = () => {

        this._changePage(this.props.page + 1);
    }

    /**
     * Event handler for previous page
     * @return {Void}
     */
    _handlePrevClick = () => {

        if (this.props.page === 1) {
            return null;
        }

        this._changePage(this.props.page - 1);
    }

    /**
     * Return true if previous button should be enabled
     * @return {Boolean} Previous button enabled
     */
    _prevButtonEnabled = () => this.props.page !== 1;

    /**
     * Return true if next button should be enabled
     * @return {Boolean} Next button enabled
     */
    _nextButtonEnabled = () => this.props.page < this.props.pageCount;

    /**
     * Change page
     * @param  {Number} pageNumber Page to set
     * @return {Void}
     */
    _changePage = pageNumber => {
        if (pageNumber !== '...') {

            this.props.changePage(pageNumber);
        }
    }

    /**
     * Render
     * @return {String}
     */
    render() {

        return (
            <div>

                <div className={styles.pages}>
                    <Button onClick={this._handlePrevClick} disabled={!this._prevButtonEnabled()}>Prev</Button>

                    {this.props.pageRange.map((page, index) => (
                        <Button
                            key={index}
                            onClick={() => this._changePage(page)}
                            size="tiny"
                            active={page === this.props.page}
                        >
                            {page}
                        </Button>
                    ))}

                    <Button onClick={this._handleNextClick} disabled={!this._nextButtonEnabled()}>Next</Button>
                </div>

            </div>
        );
    }
}

export default Pagination;
import { createSelector } from 'reselect';

import { getEntities } from './entities';
import { getInstagramUser, getAuthedUser } from './users';

export const getOrders = state => state.orders;

export const getOrdersLoading = state => state.orders.loading;
export const getOrdersLoaded = state => state.orders.loaded;
export const getSelectedOrderId = state => state.orders.selected;

export const getSkippedOrders = state => state.orders.skipped;

export const getOrdersEntities = createSelector(
	getEntities,
	getOrders,
	(entities, orders) => {

		return orders.ids.map(id => entities.orders[id]);
	}
);

export const getSelectedOrder = createSelector(
	getSelectedOrderId,
	getEntities,
	(id, entities) => entities.orders[id] || null
);

export const getUserOrders = createSelector(
    getEntities,
	getAuthedUser,
    (entities, user) => {

		const { orders } = entities;

		if (!user || !orders) {

			return [];
		}

		return user.orders.map(id => orders[id]);
    }
);

export const getOrdersToFollow = createSelector(
	getOrdersEntities,
	getInstagramUser,
	(orders, instagram) => {

		return orders.filter(order => {

			return order.orderType === 'followers' &&
				order.instagram.id.toString() !== instagram.id;
		})
	}
);

export const getOrdersToLike = createSelector(
	getOrdersEntities,
	getInstagramUser,
	(orders, instagram) => {

		return orders.filter(order => {

			return order.orderType === 'likes' &&
				order.instagram.id.toString() !== instagram.id;
		})
	}
);

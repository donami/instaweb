import { createSelector } from 'reselect';

import { getEntities } from './entities';

export const getProducts = state => state.products;

export const getProductsLoaded = state => state.products.loaded;

export const getProductEntities = createSelector(
	getEntities,
	getProducts,
	(entities, products) => {

		return products.ids.map(id => entities.products[id]);
	}
);
import { createSelector } from 'reselect';

import { getEntities } from './entities';
import { getAuthedUserId } from './users';

export const getVideos = state => state.videos;

export const getVideosLoading = state => state.videos.loading;

export const getVideosLoaded = state => state.videos.loaded;

export const getVideoEntities = createSelector(
    getEntities,
    getVideos,
    (entities, videos) => {

        if (!videos || !entities) {

            return [];
        }

        return videos.ids.map(id => entities.videos[id]);
    }
)

export const getUnwatchedVideos = createSelector(
    getVideoEntities,
    getAuthedUserId,
    (videos, userId) => videos.filter(video => video.watched.indexOf(userId) === -1)
);
import { createSelector } from 'reselect';

import { getEntities } from './entities';

export const getLogs = state => state.logs;

export const getLogsLoading = state => state.logs.loading;
export const getLogsLoaded = state => state.logs.loaded;

export const getLogsEntities = createSelector(
	getEntities,
	getLogs,
	(entities, logs) => {

		let log;

		return logs.ids.map(id => {

			log = entities.logs[id];

			return Object.assign({}, log, {
				user: entities.users[log.user],
			});
		});
	}
);

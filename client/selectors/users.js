import { createSelector } from 'reselect';

import { getEntities } from './entities';
import { getLogsEntities } from './logs';

export const getUsers = state => state.users;
export const getAuthedUserId = state => state.auth.userId;
export const getSelectedUserId = state => state.users.selected;
export const getUsersLoading = state => state.users.loading;
export const getUsersLoaded = state => state.users.loaded;

export const getAuthedUser = createSelector(
	getEntities,
	getAuthedUserId,
	(entities, userId) => {

		if (!entities || !userId || !entities.users) {
			return null;
		}

		return entities.users[userId];
	}
);

export const getUsersEntities = createSelector(
	getEntities,
	getUsers,
	(entities, users) => {

		if (!users || !users.ids) {
			return [];
		}

		return users.ids.map(id => entities.users[id]);
	}
);

export const getSelectedUser = createSelector(
	getSelectedUserId,
	getEntities,
	(id, entities) => entities.users[id] || null
);

export const getUserIsAdmin = createSelector(
	getAuthedUser,
	(user) => Boolean(user && user.userType),
);

export const getSelectedUserLogs = createSelector(
	getSelectedUserId,
	getLogsEntities,
	(userId, logs) => logs.filter(log => log.user._id === userId),
);

export const getInstagramUser = state => state.auth.instagram;

export const getToken = state => state.auth.token;
import { createSelector } from 'reselect';

import { getEntities } from './entities';

export const getTasks = state => state.tasks;

export const getTasksLoading = state => state.tasks.loading;

export const getTasksLoaded = state => state.tasks.loaded;

export const getTaskEntities = createSelector(
    getEntities,
    getTasks,
    (entities, tasks) => {

        if (!tasks || !entities) {

            return [];
        }

        return tasks.ids.map(id => entities.tasks[id]);
    }
)
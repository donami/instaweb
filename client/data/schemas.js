import { schema } from 'normalizr';

export const orderSchema = new schema.Entity('orders', {}, {
	idAttribute: '_id',
});

export const taskSchema = new schema.Entity('tasks', {}, {
	idAttribute: '_id',
});

export const productSchema = new schema.Entity('products', {}, {
	idAttribute: '_id',
});

export const userSchema = new schema.Entity('users', {
	orders: [ orderSchema ],
}, {
	idAttribute: '_id',
});

export const videoSchema = new schema.Entity('videos', {
	user: userSchema,
}, {
	idAttribute: '_id',
});

export const logSchema = new schema.Entity('logs', {
	user: userSchema,
	order: orderSchema,
	video: videoSchema,
	task: taskSchema,
}, {
	idAttribute: '_id',
});
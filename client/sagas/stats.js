import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { STATS } from '../stores/stats';
import * as actions from '../stores/stats';

export function* fetchStats() {

    try {
        const response = yield call(API.fetchStats);

        yield put(actions.statsSuccess(response));

    } catch (error) {
        yield put(actions.statsFailure(error.message));
    }
}

function* watchLoadStats() {
    yield takeEvery(STATS.REQUEST, fetchStats);
}

export default function* statsSaga() {
    yield all([
        watchLoadStats(),
    ]);
}
import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { usersSuccess, usersFailure, disableUserSuccess, disableUserFailure, restoreUserSuccess, restoreUserFailure, USERS, DISABLE_USER, RESTORE_USER } from '../stores/users';

export function* fetchUsers() {

    try {
        const response = yield call(API.fetchUsers)

        const { result, entities } = response;

        yield put(usersSuccess({ ids: result, entities }));

    } catch (error) {

        yield put(usersFailure(error.message));
    }
}

export function* disableUser({ payload: { id } }) {

    try {

        const response = yield call(API.disableUser, { id })

        yield put(disableUserSuccess(response));

    } catch (error) {

        yield put(disableUserFailure(error.message));
    }
}

export function* restoreUser({ payload: { id } }) {

    try {

        const response = yield call(API.restoreUser, { id })

        yield put(restoreUserSuccess(response));

    } catch (error) {

        yield put(restoreUserFailure(error.message));
    }
}

export default function* logSaga() {
    yield all([

        yield takeEvery(RESTORE_USER.REQUEST, restoreUser),

        yield takeEvery(DISABLE_USER.REQUEST, disableUser),

        yield takeEvery(USERS.REQUEST, fetchUsers),
    ]);
}
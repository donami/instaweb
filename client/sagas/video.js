import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { receiveCoins } from '../stores/auth';
import { VIDEO_FINISHED, VIDEOS, VIDEOS_ADD, VIDEOS_REMOVE, VIDEOS_UPDATE } from '../stores/videos';
import * as actions from '../stores/videos';
import * as uiActions from '../stores/ui';
import * as logActions from '../stores/logs';

export function* loadVideos() {

    try {

        const response = yield call(API.fetchVideos)

        const { result, entities } = response;

        yield put(actions.videosSuccess({ videos: result, entities }));

    } catch(error) {

        yield put(actions.videosFailure(error));
    }
}

export function* addVideo({ payload: { video } }) {

    try {

        const response = yield call(API.addVideo, { video });

        const { result, entities } = response;

        yield all([

            put(actions.videosAddSuccess(entities, result)),

            put(logActions.logsCreateRequest({
                message: 'Video added',
                eventType: 'video_created',
                user: video.user,
                video: result,
            })),

            put(uiActions.addMessage({
                type: 'success',
                text: 'The video was successfully added'
            })),
        ]);

    } catch (error) {

        yield put(actions.videosAddFailure(error.message));
    }
}

export function* updateVideo({ payload }) {

    try {

        const response = yield call(API.updateVideo, { videoId: payload.videoId, video: payload.video });

        const { result, entities } = response;

        yield all([
            put(actions.videosUpdateSuccess(entities, result)),

            put(logActions.logsCreateRequest({
                message: 'Video updated',
                eventType: 'video_updated',
                user: payload.video.user,
            })),

            put(uiActions.addMessage({
                type: 'success',
                text: 'The video was successfully updated'
            })),
        ]);

    } catch (error) {

        yield put(actions.videosUpdateFailure(error.message));
    }
}

export function* removeVideo({ payload }) {

    try {

        yield call(API.removeVideo, { videoId: payload.videoId });

        yield put(actions.videosRemoveSuccess(payload.videoId));

    } catch (error) {

        yield put(actions.videosRemoveFailure(error.message));
    }
}

export function* videoFinished({ payload }) {

    try {

        const { result, entities } = yield call(API.claimReward, {
            coins: payload.coins,
            userId: payload.userId,
            type: payload.earnType,
            objectId: payload.objectId
        });

        yield all([
            put(receiveCoins(payload.coins, payload.userId)),
            put(actions.videoFinishedSuccess(entities)),
        ]);

    } catch (error) {

        yield put(actions.videoFinishedFailure(error.message));
    }
}

function* watchLoadVideos() {
    yield takeEvery(VIDEOS.REQUEST, loadVideos);
}

function* watchAddVideo() {
    yield takeEvery(VIDEOS_ADD.REQUEST, addVideo);
}

function* watchUpdateVideo() {
    yield takeEvery(VIDEOS_UPDATE.REQUEST, updateVideo);
}

function* watchRemoveVideo() {
    yield takeEvery(VIDEOS_REMOVE.REQUEST, removeVideo);
}

function* watchVideoFinished() {
    yield takeEvery(VIDEO_FINISHED, videoFinished);
}

export default function* videoSaga() {
    yield all([
        watchLoadVideos(),
        watchAddVideo(),
        watchUpdateVideo(),
        watchRemoveVideo(),
        watchVideoFinished(),
    ]);
}
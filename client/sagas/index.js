import { all } from 'redux-saga/effects';

import orderSaga from './order';
import authSaga from './auth';
import videoSaga from './video';
import taskSaga from './task';
import productSaga from './product';
import logSaga from './log';
import userSaga from './user';
import statsSaga from './stats';

export default function* rootSaga() {
    yield all([
        orderSaga(),
        authSaga(),
        videoSaga(),
        taskSaga(),
        productSaga(),
        logSaga(),
        userSaga(),
        statsSaga(),
    ]);
}
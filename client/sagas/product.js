import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { PRODUCTS, PRODUCTS_CREATE, PRODUCTS_UPDATE, PRODUCTS_REMOVE } from '../stores/products';
import * as actions from '../stores/products';

export function* loadProducts() {

    try {

        const { result, entities } = yield call(API.fetchProducts);

        yield put(actions.productsSuccess({ ids: result, entities }));
    } catch (error) {

        yield put(actions.productsFailure(error.message));
    }
}

export function* createProduct({ payload: { product } }) {

    try {

        const response = yield call(API.createProduct, product);

        yield put(actions.productsCreateSuccess(response));

    } catch (error) {

        yield put(actions.productsCreateFailure(error.message));
    }
}

export function* updateProduct({ payload: { productId, product } }) {

    try {

        const response = yield call(API.updateProduct, productId, product);

        yield put(actions.productsUpdateSuccess(response));

    } catch (error) {

        yield put(actions.productsUpdateFailure(error.message));
    }
}

export function* removeProduct({ payload: { productId } }) {

    try {

        const response = yield call(API.removeProduct, productId);

        yield put(actions.productsRemoveSuccess(response));

    } catch (error) {

        yield put(actions.productsRemoveFailure(error.message));
    }
}

function* watchLoadProducts() {
    yield takeEvery(PRODUCTS.REQUEST, loadProducts);
}

function* watchCreateProduct() {
    yield takeEvery(PRODUCTS_CREATE.REQUEST, createProduct);
}

function* watchUpdateProduct() {
    yield takeEvery(PRODUCTS_UPDATE.REQUEST, updateProduct);
}

function* watchRemoveProduct() {
    yield takeEvery(PRODUCTS_REMOVE.REQUEST, removeProduct);
}

export default function* productSaga() {
    yield all([
        watchLoadProducts(),
        watchCreateProduct(),
        watchUpdateProduct(),
        watchRemoveProduct(),
    ]);
}
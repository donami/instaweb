import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { TASKS, TASKS_CREATE, TASKS_UPDATE, TASKS_REMOVE } from '../stores/tasks';
import * as actions from '../stores/tasks';
import * as uiActions from '../stores/ui';
import * as logActions from '../stores/logs';

export function* loadTasks() {

    try {
        const response = yield call(API.fetchTasks)

        const { result, entities } = response;

        yield put(actions.tasksSuccess({tasks: result, entities}));

    } catch (error) {

        yield put(actions.tasksFailure(error));
    }
}

export function* createTask({ payload: { task } }) {

    try {
        const response = yield call(API.createTask, task);

        yield all([
            put(actions.tasksCreateSuccess(response)),

            put(logActions.logsCreateRequest({
                message: 'Task added',
                eventType: 'task_created',
                user: task.user,
                task: response.result,
            })),

            put(uiActions.addMessage({
                type: 'success',
                text: 'The task was successfully added'
            })),
        ]);
    } catch (error) {

        yield put(actions.tasksCreateFailure(error.message));
    }
}

export function* updateTask({ payload }) {

    try {

        const response = yield call(API.updateTask, { taskId: payload.taskId, task: payload.task });

        const { result, entities } = response;

        yield all([
            put(actions.tasksUpdateSuccess(entities, result)),

            put(logActions.logsCreateRequest({
                message: 'Task updated',
                eventType: 'task_updated',
                user: payload.task.user,
            })),

            put(uiActions.addMessage({
                type: 'success',
                text: 'The task was successfully updated'
            })),
        ]);

    } catch (error) {

        yield put(actions.tasksUpdateFailure(error.message));
    }
}

export function* removeTask({ payload }) {

    try {

        yield call(API.removeTask, { taskId: payload.taskId });

        yield put(actions.tasksRemoveSuccess(payload.taskId));

    } catch (error) {

        yield put(actions.tasksRemoveFailure(error.message));
    }
}

function* watchLoadTasks() {
    yield takeEvery(TASKS.REQUEST, loadTasks);
}

function* watchCreateTask() {
    yield takeEvery(TASKS_CREATE.REQUEST, createTask);
}

function* watchUpdateTask() {
    yield takeEvery(TASKS_UPDATE.REQUEST, updateTask);
}

function* watchRemoveTask() {
    yield takeEvery(TASKS_REMOVE.REQUEST, removeTask);
}

export default function* taskSaga() {
    yield all([
        watchLoadTasks(),
        watchCreateTask(),
        watchUpdateTask(),
        watchRemoveTask(),
    ]);
}
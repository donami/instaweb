import { put, takeEvery, all, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import API from '../services/api';
import * as instagramAPI from '../services/instagram';
import * as actions from '../stores/auth';
import { USER_MEDIA, DAILY_CHECKIN, RECEIVE_TOKEN, RECEIVE_INSTAGRAM_USER, RECEIVE_FOLLOWING, RECEIVE_LIKES, RECEIVE_USER, RECEIVE_TOKEN_FAIL, DAILY_CHECKIN_CHECK, DAILY_CHECKIN_CLEARED } from '../stores/auth';

function* receiveToken({ payload }) {

    const { token } = payload;

    try {

        const instagramUser = yield call(instagramAPI.getUserSelf, token);

        const { user, following, likes } = yield all({
            user: call(API.fetchUser, instagramUser.username),
            following: call(instagramAPI.getFollowing, token),
            likes: call(instagramAPI.getLikes, token),
        });

        const followingIds = following.data.map(following => following.id);
        const likesIds = likes.data.map(like => like.id);

        const { entities, result } = user;

        const fetchedUser = entities.users[result];

        // Halt if the user has been disabled
        if (fetchedUser.disabled) {
            throw new Error('This account has been disabled.');
        }

        yield all([
            put(actions.receiveInstagramUser(instagramUser, token)),
            put(actions.receiveFollowing(followingIds)),
            put(actions.receiveLikes(likesIds)),
            put(actions.receiveUser(fetchedUser, result, entities)),
        ]);

        yield put(push('/?authed'));
    } catch (error) {

        yield put(actions.receiveTokenFail(error.message));
    }
}

function* loadUserMedia({ payload }) {

    const { token } = payload;

    try {
        const { data } = yield call(instagramAPI.getUserMedia, token);

        yield put(actions.userMediaSuccess(data));
    } catch (error) {

        yield put(actions.userMediaFailure(error.message));
    }
}

function* checkDaily({ payload: { dates, checkins, userId } }) {

    if (checkins.indexOf(dates.yesterday) === -1 && checkins.indexOf(dates.today) === -1) {

        const { result, entities } = yield call(API.dailyCheckinsClear, userId);

        yield put(actions.dailyCheckinCleared(userId));
    }
}

function* dailyCheckin({ payload: { dates, userId, rewards, instagram } }) {

    try {
        const { entities, result } = yield call(API.dailyCheckin, dates, userId, rewards, instagram);

        const user = entities.users[result];

        yield put(actions.dailyCheckinSuccess(user.checkins, userId, entities));
    } catch (error) {

        yield put(actions.dailyCheckinFailure(error.message));
    }
}

function* watchCheckDaily() {
    yield takeEvery(DAILY_CHECKIN_CHECK, checkDaily);
}

function* watchDailyCheckin() {
    yield takeEvery(DAILY_CHECKIN.REQUEST, dailyCheckin);
}

function* watchLoadUserMedia() {
    yield takeEvery(USER_MEDIA.REQUEST, loadUserMedia);
}

function* watchReceiveToken() {
    yield takeEvery(RECEIVE_TOKEN, receiveToken);
}

export default function* authSaga() {
  yield all([
    watchCheckDaily(),
    watchDailyCheckin(),
    watchLoadUserMedia(),
    watchReceiveToken(),
  ])
}
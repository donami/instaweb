import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { ordersSuccess, ordersFailure, orderCreateSuccess, orderCreateFailure, likeImageSuccess, likeImageFailure, followUserSuccess, followUserFailure, ORDERS, ORDER_CREATE, FOLLOW_USER, LIKE_IMAGE } from '../stores/orders';
import * as uiActions from '../stores/ui';
import { removeCoins, receiveCoins } from '../stores/auth';
import * as logActions from '../stores/logs';
import * as instagramAPI from '../services/instagram';

/**
 * Fetches all orders
 * @return {Generator}
 */
export function* loadOrders() {

    try {
        const response = yield call(API.fetchOrders)

        yield put(ordersSuccess(response));
    } catch (error) {
        yield put(ordersFailure(error));
    }
}

/**
 * Create order generator
 * @param  {Object}    order Data for the order to be created
 * @return {Generator}
 */
function* createOrder({ payload: { order } }) {

    const { coins, userId } = order;

    const user = yield call(API.getUserById, userId);

    try {
        if (user.coins < coins) {

            throw new Error('Insufficient coins');
        }

        if (!user) {

            throw new Error('Unable to find user.');
        }

        const newOrder = yield call(API.createOrder, order);

        const { entities, result } = newOrder;

        yield all([
            put(orderCreateSuccess({ id: result, entities })),
            put(removeCoins(coins, userId)),
            put(logActions.logsCreateRequest({
                message: 'The event',
                eventType: 'order_create',
                user: userId,
                order: result,
            })),
            put(uiActions.addMessage({ type: 'success', text: 'Your order was created successfully' })),
        ]);

    } catch(error) {

        yield put(orderCreateFailure(error.message));
    }
}

/**
 * Like image generator
 * @param  {Object}    data { reward: Int, userId: String}
 * @return {Generator}
 */
export function* likeImage({ payload }) {

    try {

        const order = yield call(API.likeImage, payload);

        yield all([
            put(likeImageSuccess(order)),
            put(receiveCoins(payload.reward, payload.userId)),
        ]);

    } catch (error) {

        yield put(likeImageFailure(error.message));
    }
}

/**
 * Follow user generator
 * @param  {Object}    order  The order the user who is getting followed created
 * @param  {Object}    auth   Auth
 * @param  {Integer}   reward The amount of coins that the user will receieve
 * @return {Generator}
 */
export function* followUser({ payload: { order, auth, reward } }) {

    try {
        const following = yield call(instagramAPI.getFollowing, auth.token);

        if (following.data.find(user => parseInt(user.id) === parseInt(order.instagram.id))) {

            throw new Error('You are already following this user.');
        }

        yield all([
            call(instagramAPI.followUser, order.instagram.id, auth.token),
            call(API.followUser, order._id, auth.userId),
        ]);

        yield all([
            put(followUserSuccess(order.instagram.id)),
            put(receiveCoins(reward, auth.userId)),
        ]);

    } catch (error) {

        yield put(followUserFailure(error.message));
    }
}

function* watchLoadOrders() {
    yield takeEvery(ORDERS.REQUEST, loadOrders);
}

function* watchCreateOrder() {
    yield takeEvery(ORDER_CREATE.REQUEST, createOrder);
}

function* watchLikeImage() {
    yield takeEvery(LIKE_IMAGE.REQUEST, likeImage);
}

function* watchFollowUser() {
    yield takeEvery(FOLLOW_USER.REQUEST, followUser);
}

export default function* orderSaga() {
    yield all([
        watchLoadOrders(),
        watchCreateOrder(),
        watchLikeImage(),
        watchFollowUser(),
    ]);
}

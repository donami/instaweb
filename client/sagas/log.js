import { put, takeEvery, all, call } from 'redux-saga/effects';

import API from '../services/api';
import { logsSuccess, logsFailure, logsCreateSuccess, logsCreateFailure, LOGS, LOGS_CREATE } from '../stores/logs';

export function* fetchLogs() {

    try {
        const response = yield call(API.fetchLogs)

        const { result, entities } = response;

        yield put(logsSuccess({ids: result, entities}));

    } catch (error) {
        yield put(logsFailure(error.message));
    }
}

export function* createLog({ payload: { log } }) {

    try {
        const response = yield call(API.createLog, log)

        const { result, entities } = response;

        yield put(logsCreateSuccess({ ids: result, entities }));

    } catch (error) {

        yield put(logsCreateFailure(error.message));
    }
}

function* watchLoadLogs() {
    yield takeEvery(LOGS.REQUEST, fetchLogs);
}

function* watchCreateLog() {
    yield takeEvery(LOGS_CREATE.REQUEST, createLog);
}

export default function* logSaga() {
    yield all([
        watchLoadLogs(),
        watchCreateLog(),
    ]);
}
import request from 'superagent';
import { normalize } from 'normalizr';

import { userSchema, orderSchema, taskSchema, videoSchema, productSchema, logSchema } from '../../data/schemas';

let URL = 'http://46.101.62.83:3001/api';

if (process.env.NODE_ENV !== 'production') {

	URL = 'http://localhost:3001/api';
}

function callApi(endpoint, schema) {

	const fullUrl = (endpoint.indexOf(URL) === -1) ? URL + endpoint : endpoint

	return request
		.get(fullUrl)
		.then(
			response => normalize(response.body, schema),
			error => {

				if (error.response && error.response.body) {
					return Promise.reject(error.response.body);
				}
				return Promise.reject(error);
			}
		)
}

const fetchUser = username => callApi(`/user/find?username=${username}`, userSchema)

const fetchUsers = () => callApi('/user', [userSchema]);

const fetchOrders = () => callApi('/order', [orderSchema]);

const fetchTasks = () => callApi('/task', [taskSchema]);

const fetchVideos = () => callApi('/video', [videoSchema]);

const fetchProducts = () => callApi('/product', [productSchema]);

const fetchLogs = () => callApi('/log', [logSchema]);

const fetchStats = () => {

	return request.get(`${URL}/stats`)
	.then(res => res.body);
}

const followUser = (orderId, userId) => {

	return request.post(`${URL}/order/follow-user`)
	.send({ orderId, userId })
	.then(res => res.body);
}

const likeImage = data => {
	const { orderId, userId, mediaId, token } = data;

	return request.post(`${URL}/order/like-image`)
	.send({ orderId, userId, mediaId, token })
	.then(
		res => res.body,
		err => Promise.reject(err),
	);
}

const disableUser = ({ id }) => {
	return request.post(`${URL}/user/disable`)
	.send({ userId: id })
	.then(res => normalize(res.body, userSchema));
}

const addVideo = ({ video }) => {
	return request.post(`${URL}/video`)
	.send({ video })
	.then(res => normalize(res.body, videoSchema));
}

const removeVideo = ({ videoId }) => {
	return request.delete(`${URL}/video/${videoId}`)
	.then(res => res.body);
}

const updateVideo = ({ videoId, video }) => {
	return request.put(`${URL}/video/${videoId}`)
	.send({ video })
	.then(res => normalize(res.body, videoSchema));
}

const removeTask = ({ taskId }) => {
	return request.delete(`${URL}/task/${taskId}`)
	.then(res => res.body);
}

const removeProduct = (productId) => {
	return request.delete(`${URL}/product/${productId}`)
	.then(res => res.body);
}

const updateTask = ({ taskId, task }) => {
	return request.put(`${URL}/task/${taskId}`)
	.send({ task })
	.then(res => normalize(res.body, taskSchema));
}

const restoreUser = ({ id }) => {
	return request.post(`${URL}/user/disable`)
	.send({ userId: id, disable: false })
	.then(res => normalize(res.body, userSchema));
}

const claimReward = ({ coins, userId, type, objectId }) => {
	return request.post(`${URL}/user/claim-reward`)
	.send({ coins, userId, type, objectId })
	.then(res => normalize(res.body, videoSchema));
}

const dailyCheckinsClear = userId => {
	return request.post(`${URL}/user/daily-checkin-clear`)
	.send({ userId })
	.then(res => normalize(res.body, userSchema));
}

const dailyCheckin = (dates, userId, rewards, instagram) => {
	return request.post(`${URL}/user/daily-checkin`)
	.send({ dates, userId, rewards, instagram })
	.then(res => normalize(res.body, userSchema));
}

const createLog = log => {
	return request.post(`${URL}/log`)
	.send(log)
	.then(res => normalize(res.body, logSchema));
}

const getUserById = userId => {
	return request.get(`${URL}/user/id/${userId}`)
	.then(res => res.body);
}

const createOrder = data => {
	return request.post(`${URL}/order`)
	.send({
		orderType: data.type,
		amount: data.amount,
		instagram: data.instagram,
		userId: data.userId,
		media: data.media,
		coins: data.coins,
	})
	.then(res => normalize(res.body, userSchema));
}

const createTask = data => {
	return request.post(`${URL}/task`)
	.send({
		name: data.name,
		url: data.url,
		coins: data.coins,
		user: data.user,
	})
	.then(res => normalize(res.body, taskSchema));
}

const createProduct = product => {
	return request.post(`${URL}/product`)
	.send(product)
	.then(res => normalize(res.body, productSchema));
}

const updateProduct = (productId, product) => {
	return request.put(`${URL}/product/${productId}`)
	.send({ product })
	.then(res => normalize(res.body, productSchema));
}

export default {
	url: URL,
	fetchUser,
	fetchUsers,
	fetchOrders,
	fetchStats,
	getUserById,
	followUser,
	likeImage,
	disableUser,
	restoreUser,
	claimReward,
	createOrder,
	createLog,
	createTask,
	createProduct,
	addVideo,
	updateVideo,
	removeVideo,
	removeProduct,
	updateTask,
	updateProduct,
	removeTask,
	fetchTasks,
	fetchVideos,
	fetchProducts,
	fetchLogs,
	dailyCheckin,
	dailyCheckinsClear,
};
import request from 'superagent';

let API = 'http://46.101.62.83:3001/api';

if (process.env.NODE_ENV !== 'production') {

	API = 'http://localhost:3001/api';
}

export const CLIENT_ID = '7b416d959451474381bb7aada925ef68';
export const CLIENT_SECRET = 'ab2f8c40b7f744a0ac915efbd93aa687';
// export const REDIRECT_URI = `http://localhost:8080/handle-auth`;
export const REDIRECT_URI = `${window.location.origin}/handle-auth`;

export const getFollowing = token => {

    return request.get(`https://api.instagram.com/v1/users/self/follows?access_token=${token}`)
    .then(res => res.body);
}

export const getLikes = token => {

    return request.get(`https://api.instagram.com/v1/users/self/media/liked?access_token=${token}`)
    .then(res => res.body);
}

export const followUser = (userId, token) => {

    return request.get(`${API}/user/follow?token=${token}&userId=${userId}`)
    .then(res => res.body);
}

export const likeMedia = (mediaId, token) => {

    return request.post(`https://api.instagram.com/v1/media/${mediaId}/likes`)
    .send({ access_token: token })
    .then(res => res.body);
}

export const getUserMedia = token => {
    return request.get(`https://api.instagram.com/v1/users/self/media/recent/?access_token=${token}`)
    .then(res => res.body);
}

export const getUserSelf = token => {
    return request.get(`https://api.instagram.com/v1/users/self/?access_token=${token}`)
    .timeout({ deadline: 6000 })
    .then(res => res.body.data);
}
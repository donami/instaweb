export const ENABLED_MODULES = {
    earn: true,
    dailyCheckin: true,
};

export const FOLLOW_USER_COIN_REWARD = 10;

export const LIKE_MEDIA_COIN_REWARD = 5;

export const DAILY_CHECKIN_REWARDS = [
    { type: 'coins', value: 10 },
    { type: 'coins', value: 20 },
    { type: 'followers', value: 5 },
    { type: 'coins', value: 30 },
    { type: 'coins', value: 40 },
    { type: 'followers', value: 10 },
    { type: 'followers', value: 15 },
    { type: 'coins', value: 50 },
    { type: 'coins', value: 60 },
    { type: 'coins', value: 70 },
    { type: 'followers', value: 20 },
    { type: 'coins', value: 80 },
    { type: 'coins', value: 90 },
    { type: 'coins', value: 95 },
    { type: 'followers', value: 25 },
    { type: 'coins', value: 100 },
    { type: 'coins', value: 105 },
    { type: 'coins', value: 110 },
    { type: 'coins', value: 115 },
    { type: 'followers', value: 30 },
    { type: 'coins', value: 120 },
    { type: 'coins', value: 125 },
    { type: 'coins', value: 130 },
    { type: 'coins', value: 135 },
    { type: 'followers', value: 35 },
    { type: 'coins', value: 140 },
    { type: 'coins', value: 145 },
    { type: 'coins', value: 150 },
    { type: 'coins', value: 155 },
    { type: 'followers', value: 40 },
    { type: 'coins', value: 200 },
];
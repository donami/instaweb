import { testSaga } from 'redux-saga-test-plan';

import API from '../client/services/api';
import { disableUser, restoreUser } from '../client/sagas/user';

describe ('userSaga', () => {

    describe ('disableUser', () => {

        it ('should execute all actions', () => {

            const userId = 1;

            const input = {
                payload: {
                    id: userId
                },
            };

            const apiResponse = {
                entities: {
                    users: {
                        [userId]: {
                            disabled: true,
                        }
                    }
                },
                result: [userId],
            };

            testSaga(disableUser, input)
                .next()
                .call(API.disableUser, { id: userId })
                .next(apiResponse)
                .put({ type: 'DISABLE_USER_SUCCESS',
                    payload: {
                        response: apiResponse,
                    },
                })
                .next()
                .isDone();
        });
    });

    describe ('restoreUser', () => {

        it ('should execute all actions', () => {

            const userId = 1;

            const input = {
                payload: {
                    id: userId
                },
            };

            const apiResponse = {
                entities: {
                    users: {
                        [userId]: {
                            disabled: false,
                        }
                    }
                },
                result: [userId],
            };

            testSaga(restoreUser, input)
                .next()
                .call(API.restoreUser, { id: userId })
                .next(apiResponse)
                .put({ type: 'RESTORE_USER_SUCCESS',
                    payload: {
                        response: apiResponse,
                    },
                })
                .next()
                .isDone();
        });
    });
});
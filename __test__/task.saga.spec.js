import { call, put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import API from '../client/services/api';
import { loadTasks, createTask } from '../client/sagas/task';

describe ('taskSaga', () => {

    describe ('loadTasks', () => {

        it('fetches the tasks', () => {

            return expectSaga(loadTasks, API)
            .provide([
                [call(API.fetchTasks), { entities: {}, result: [] }],
            ])
            .put({
                type: 'TASKS_SUCCESS',
                payload: {
                    response: { tasks: [], entities: {} },
                },
            })
            .dispatch({ type: 'TASKS_REQUEST' })
            .run();
        });

        it ('handles errors', () => {

            const error = new Error('error');

            return expectSaga(loadTasks, API)
            .provide([
                [matchers.call.fn(API.fetchTasks), throwError(error)]
            ])
            .put({
                type: 'TASKS_FAILURE',
                payload: error,
                error: true,
            })
            .dispatch({ type: 'TASKS_REQUEST' })
            .run();
        });

    });

    describe('createTask', () => {

        const task = {
            id: 1,
            name: 'Fake task',
            url: 'http://',
            coins: 10,
            user: '1bc2143bc4343'
        };

        it ('dispatches tasksSuccess', () => {

            const apiResponse = {
                entities: {
                    tasks: {
                        [1]: task,
                    }
                },
                result: 1,
            }

            return expectSaga(createTask, { payload: { task } })
                .provide([
                    [matchers.call.fn(API.createTask), apiResponse]
                ])
                .put({
                    type: 'TASKS_CREATE_SUCCESS',
                    payload: {
                        response: apiResponse,
                    },
                })
                .dispatch('TASKS_CREATE_REQUEST')
                .run();
        });

        it ('handles error', () => {

            const err = new Error('error');

            return expectSaga(createTask, { payload: { task } })
                .provide([
                    [matchers.call.fn(API.createTask), throwError(err)]
                ])
                .put({
                    type: 'TASKS_CREATE_FAILURE',
                    payload: {
                        error: err.message,
                    },
                })
                .dispatch('TASKS_CREATE_REQUEST')
                .run();
        })
    })
});
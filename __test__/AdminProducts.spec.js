import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import { AdminProducts } from '../client/components/AdminProducts';

describe ('AdminProducts component', () => {

    let wrapper;

    it ('should render', () => {

        wrapper = shallow(<AdminProducts />);

        expect(wrapper.length).toEqual(1);
    })

    it ('should contain AdminProductsList component', () => {

        wrapper = mount(<AdminProducts />);

        expect(wrapper.find('AdminProductsList').length).toEqual(1);
        expect(wrapper.find('AdminProductsList').prop('products')).toEqual(wrapper.props().products);
    })

    it ('should contain AdminProductsForm component', () => {

        wrapper = shallow(<AdminProducts />);

        expect(wrapper.find('AdminProductsForm').length).toEqual(1);
    })

    it ('loads products if not loaded', () => {

        const loadProductsSpy = sinon.spy();

        wrapper = mount(<AdminProducts loaded={false} loadProducts={loadProductsSpy} />);

        expect(loadProductsSpy.calledOnce).toEqual(true);
    })

})
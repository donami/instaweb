import React from 'react';
import { shallow } from 'enzyme';

import Tasks from '../client/components/tasks';

describe('Tasks component', () => {

    let wrapper;

    const minProps = {
        tasks: [],
    };

    it ('renders a list', () => {

        wrapper = shallow(<Tasks {...minProps} />);

        expect(wrapper.find('ul').length).toEqual(1);
        expect(wrapper.find('li').length).toEqual(0);
    });

    it ('has items', () => {

        const tasks = [
            {
                _id: '5cb66c433',
                name: 'fake task',
            },
            {
                _id: '566b66b6bc',
                name: 'fake task 2',
            },
        ];

        wrapper = shallow(<Tasks {...minProps} tasks={tasks} />);

        expect(wrapper.find('li').length).toEqual(2);
    });
});
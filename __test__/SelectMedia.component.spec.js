import React from 'react';
import { shallow } from 'enzyme';

import SelectMedia from '../client/components/SelectMedia';

describe ('SelectMedia component', () => {

    let wrapper;

    const minProps = {
        selectImage: () => {},
        selectedImage: {
            id: '1',
        },
        pagination: {},
        changePage: () => {},
        images: [
            {
                id: 1,
                images: {
                    thumbnail: {
                        url: 'image url',
                    }
                }
            }
        ],
    };

    it ('renders', () => {

        wrapper = shallow(<SelectMedia {...minProps} />);

        expect(wrapper.length).toEqual(1);
    });
});
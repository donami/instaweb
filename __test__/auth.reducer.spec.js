import auth from '../client/stores/auth';

const FAKE_MEDIAS = [
    { id: 1, type: 'image' },
    { id: 2, type: 'videos' },
];

describe('auth reducer', () => {

    let state;

    beforeEach(()=>{
        state = {
            mediaLoaded: false,
            following: [],
            liked: [],
            media: [],
            token: null,
            userId: null,
        };
    });

    describe ('RECEIVE_TOKEN', () => {

        it ('should set token value', () => {

            state = auth(state, {
                type: 'RECEIVE_TOKEN',
                payload: { token: '12321abc.12334c.12331' }
            });

            expect(state.token).toEqual('12321abc.12334c.12331');
        });
    });

    describe('RECEIVE_USER', () => {

        it ('should set media loaded to true when loaded', () => {

            state = auth(state, {
                type: 'USER_MEDIA_SUCCESS',
                payload: {
                    media: FAKE_MEDIAS,
                },
            });

            expect(state.mediaLoaded).toEqual(true);
            expect(state.media).toEqual(FAKE_MEDIAS);
        });
    });

    describe('RECEIVE_USER', () => {

        it ('should set the userId', () => {

            state = auth(state, {
                type: 'RECEIVE_USER',
                payload: {
                    userId: '17va621',
                },
            });

            expect(state.userId).toEqual('17va621');
        });
    });

    describe('RECEIVE_LIKES', () => {

        it ('should add the ids to liked', () => {

            state = auth(state, {
                type: 'RECEIVE_LIKES',
                payload: {
                    ids: [1, 2, 3],
                },
            });

            expect(state.liked).toEqual([1, 2, 3]);
        });
    });

    describe('FOLLOW_USER_SUCCESS', () => {

        it ('should add to instagram id to following', () => {

            state = auth(state, {
                type: 'FOLLOW_USER_SUCCESS',
                payload: {
                    instagramId: 1,
                },
            });

            expect(state.following).toEqual(['1']);
        });
    });

    describe('LIKE_IMAGE_SUCCESS', () => {

        it ('should add the media id to liked, and convert to string', () => {

            state = auth(state, {
                type: 'LIKE_IMAGE_SUCCESS',
                payload: {
                    order: { media: { id: 1 } },
                },
            });

            expect(state.liked).toEqual(['1']);
        });
    });

    describe('RECEIVE_INSTAGRAM_USER', () => {

        it ('should assign instagram and token values', () => {

            state = auth(state, {
                type: 'RECEIVE_INSTAGRAM_USER',
                payload: {
                    instagram: { username: 'fakeuser' },
                    token: '12334c.12321abc.1231',
                },
            });

            expect(state.instagram).toEqual({ username: 'fakeuser' });
            expect(state.token).toEqual('12334c.12321abc.1231');
        });
    });
})
import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import API from '../client/services/api';
import { loadVideos } from '../client/sagas/video';

describe ('videoSaga', () => {

    it ('fetches the videos', () => {

        return expectSaga(loadVideos, API)
            .provide([
                [call(API.fetchVideos), { entities: {}, result: [] }],
            ])
            .put({
                type: 'VIDEOS_SUCCESS',
                payload: {
                    response: { videos: [], entities: {} },
                },
            })
            .dispatch({ type: 'VIDEOS_REQUEST' })
            .run();
    });

    it ('handles errors', () => {

        const error = new Error('error');

        return expectSaga(loadVideos, API)
            .provide([
                [matchers.call.fn(API.fetchVideos), throwError(error)]
            ])
            .put({
                type: 'VIDEOS_FAILURE',
                error: true,
                payload: error,
            })
            .dispatch({ type: 'VIDEOS_REQUEST' })
            .run();
    });
});
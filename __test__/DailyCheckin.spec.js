import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import sinon from 'sinon';

import { DailyCheckin } from '../client/containers/DailyCheckin';

describe('DailyCheckin component', () => {

    let wrapper;

    const minProps = {
        auth: {
            _id: '5b12311c231cb1234',
            checkins: [],
        },
        instagram: {},
        dailyCheckinRequest: () => {},
        dailyCheckinCheck: () => {},
    };

    it ('should contain a checkin button', () => {

        wrapper = shallow(<DailyCheckin {...minProps} />);

        expect(wrapper.find('Button').length).toEqual(1);
    });

    it ('renders a disabled button if already checked in', () => {

        wrapper = shallow(<DailyCheckin {...minProps} />);

        expect(wrapper.find('Button').prop('disabled')).toBe(undefined);

        const today = moment().format('YYYY-MM-DD');

        let auth = {
            checkins: [today],
        };

        wrapper = shallow(<DailyCheckin {...minProps} auth={auth} />);

        expect(wrapper.find('Button').prop('disabled')).toEqual(true);
    });

    it ('calls componentDidMount', () => {
        sinon.spy(DailyCheckin.prototype, 'componentDidMount');

        wrapper = mount(<DailyCheckin {...minProps} />);
        expect(DailyCheckin.prototype.componentDidMount.calledOnce).toEqual(true);
    });

    it ('dispatches dailyCheckinCheck action', () => {

        const dailyCheckinCheckSpy = sinon.spy();

        wrapper = mount(<DailyCheckin {...minProps} dailyCheckinCheck={dailyCheckinCheckSpy}/>);

        expect(dailyCheckinCheckSpy.calledOnce).toEqual(true);
    })
})
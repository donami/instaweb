import tasks from '../client/stores/tasks';

describe('tasks reducer', () => {

    let state;

    beforeEach(()=>{
        state = {
            ids: [],
            skipped: [],
            loaded: false,
            loading: false,
            fetchedAt: null,
        };
    });

    describe ('TASKS_CREATE_REQUEST', () => {

        it ('should set loading to true', () => {
            state = tasks(state, {
                type: 'TASKS_CREATE_REQUEST',
            });

            expect(state.loading).toEqual(true);
            expect(state.loaded).toEqual(false);
        });
    })

    describe ('TASKS_CREATE_FAILURE', () => {

        it ('should set loading and loaded to false', () => {

            state = tasks(state, {
                type: 'TASKS_CREATE_FAILURE',
            });

            expect(state.loading).toEqual(false);
            expect(state.loaded).toEqual(false);
        })
    });

    describe ('TASKS_CREATE_SUCCESS', () => {

        it ('should add the id of the new task to ids', () => {

            state = tasks(state, {
                type: 'TASKS_CREATE_SUCCESS',
                payload: {
                    response: {
                        result: '5ab71231',
                    },
                },
            });

            expect(state.ids).toEqual(['5ab71231']);
        });
    });

    describe ('TASKS_REQUEST', () => {

        it ('should set loaded to false and loading to true', () => {

            state = tasks(state, { type: 'TASKS_REQUEST' });

            expect(state.loaded).toEqual(false);
            expect(state.loading).toEqual(true);
        });
    });

    describe ('TASKS_SUCCESS', () => {

        it ('should set loaded to true and loading to false', () => {

            state = tasks(state, {
                type: 'TASKS_SUCCESS',
                payload: {
                    response: {
                        tasks: ['13213', '435']
                    }
                },
            });

            expect(state.loaded).toEqual(true);
            expect(state.loading).toEqual(false);
        });

        it ('should add fetched task ids to ids', () => {

            state = tasks(state, {
                type: 'TASKS_SUCCESS',
                payload: {
                    response: { tasks: ['13213', '435'] },
                },
            });

            expect(state.ids).toEqual(['13213', '435']);
        });
    });

    describe ('TASKS_FAILURE', () => {

        it ('should set loaded and loading to false', () => {

            state = tasks(state, { type: 'TASKS_FAILURE' });

            expect(state.loaded).toEqual(false);
            expect(state.loading).toEqual(false);
        });
    });

})
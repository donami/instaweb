import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import API from '../client/services/api';
import { fetchLogs, createLog } from '../client/sagas/log';

describe ('logSaga', () => {

    describe ('fetchLogs', () => {

        it('fetches the logs', () => {

            return expectSaga(fetchLogs, API)
            .provide([
                [call(API.fetchLogs), { entities: {}, result: [] }],
            ])
            .put({
                type: 'LOGS_SUCCESS',
                payload: {
                    response: { ids: [], entities: {} },
                },
            })
            .dispatch({ type: 'LOGS_REQUEST' })
            .run();
        });

        it ('handles errors', () => {

            const error = new Error('error');

            return expectSaga(fetchLogs, API)
            .provide([
                [matchers.call.fn(API.fetchLogs), throwError(error)]
            ])
            .put({
                type: 'LOGS_FAILURE',
                payload: { error: error.message },
            })
            .dispatch({ type: 'LOGS_REQUEST' })
            .run();
        });
    });

    describe ('createLog', () => {

        it ('creates a log', () => {

            const fakeLog = {
                _id: '21b123ac',
                message: 'Log event',
                eventType: 'order_create',
                user: '123vc',
            };

            const expectedResponse = {
                entities: {
                    logs: {
                        ['21b123ac']: fakeLog,
                    },
                },
                result: ['21b123ac'],
            };

            return expectSaga(createLog, { payload: { log: fakeLog } })
            .provide([
                [matchers.call.fn(API.createLog), expectedResponse],
            ])
            .put({
                type: 'LOGS_CREATE_SUCCESS',
                payload: {
                    response: {
                        entities: expectedResponse.entities,
                        ids: expectedResponse.result,
                    },
                },
            })
            .dispatch({ type: 'LOGS_CREATE_REQUEST' })
            .run();
        })
    })
});
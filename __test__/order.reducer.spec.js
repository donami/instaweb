import orders from '../client/stores/orders';

describe('orders reducer', () => {

    let state;

    beforeEach(()=>{
        state = {
            ids: [],
            skipped: [],
            loaded: false,
            loading: false,
            fetchedAt: null,
        };
    });

    describe ('ORDER_CREATE_SUCCESS', () => {

        it ('should add the id of the new order to ids', () => {

            state = orders(state, {
                type: 'ORDER_CREATE_SUCCESS',
                payload: {
                    id: '5ab71231',
                },
            });

            expect(state.ids).toEqual(['5ab71231']);
        });
    });

    describe ('ORDERS_REQUEST', () => {

        it ('should set loaded to false and loading to true', () => {

            state = orders(state, { type: 'ORDERS_REQUEST' });

            expect(state.loaded).toEqual(false);
            expect(state.loading).toEqual(true);
        });
    });

    describe ('ORDERS_SUCCESS', () => {

        it ('should set loaded to true and loading to false', () => {

            state = orders(state, {
                type: 'ORDERS_SUCCESS',
                payload: {
                    response: { result: ['13213', '435'] }
                },
            });

            expect(state.loaded).toEqual(true);
            expect(state.loading).toEqual(false);
        });

        it ('should add fetched order ids to ids', () => {

            state = orders(state, {
                type: 'ORDERS_SUCCESS',
                payload: {
                    response: { result: ['13213', '435'] },
                },
            });

            expect(state.ids).toEqual(['13213', '435']);
        });
    });

    describe ('ORDERS_FAILURE', () => {

        it ('should set loaded and loading to false', () => {

            state = orders(state, { type: 'ORDERS_FAILURE' });

            expect(state.loaded).toEqual(false);
            expect(state.loading).toEqual(false);
        });
    });

    describe ('SKIP_ORDER', () => {

        it ('should add the order id of skipped order to skipped', () => {

            state = orders(state, {
                type: 'SKIP_ORDER',
                payload: {
                    order: { _id: '123' }
                },
            });

            expect(state.skipped).toEqual(['123']);
        });
    });

})
import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import API from '../client/services/api';
import { loadProducts, createProduct, updateProduct, removeProduct } from '../client/sagas/product';

describe ('productSaga', () => {

    describe ('loadProducts', () => {

        it('fetches the products', () => {

            return expectSaga(loadProducts, API)
            .provide([
                [call(API.fetchProducts), { entities: {}, result: [] }],
            ])
            .put({
                type: 'PRODUCTS_SUCCESS',
                payload: {
                    response: { ids: [], entities: {} },
                },
            })
            .dispatch({ type: 'PRODUCTS_REQUEST' })
            .run();
        });

        it ('handles errors', () => {

            const error = new Error('error');

            return expectSaga(loadProducts, API)
            .provide([
                [matchers.call.fn(API.fetchProducts), throwError(error)]
            ])
            .put({
                type: 'PRODUCTS_FAILURE',
                payload: {
                    error: error.message,
                },
            })
            .dispatch({ type: 'PRODUCTS_REQUEST' })
            .run();
        });
    });

    describe('createProduct', () => {

        const product = {
            _id: '5bc5343c',
            name: 'fake product',
        }

        const apiResponse = {
            entities: {
                products: {
                    [product._id]: product,
                },
                result: product._id,
            },
        };

        it ('dispatches productsCreateSuccess', () => {

            return expectSaga(createProduct, { payload: { product }})
                .provide([
                    [matchers.call.fn(API.createProduct), apiResponse],
                ])
                .put({
                    type: 'PRODUCTS_CREATE_SUCCESS',
                    payload: {
                        response: apiResponse,
                    },
                })
                .dispatch('PRODUCTS_CREATE_REQUEST')
                .run();
        })

        it ('handles errors', () => {

            const error = new Error('error');

            return expectSaga(createProduct, { payload: { product }})
            .provide([
                [matchers.call.fn(API.createProduct), throwError(error)]
            ])
            .put({
                type: 'PRODUCTS_CREATE_FAILURE',
                payload: {
                    error: error.message,
                },
            })
            .dispatch({ type: 'PRODUCTS_CREATE_REQUEST' })
            .run();
        })
    });

    describe ('updateProduct', () => {

        it ('dispatches productsUpdateSuccess', () => {

            const productId = '1';

            const product = {
                _id: productId,
                name: 'fake product',
            }

            const apiResponse = {
                entities: {
                    products: {
                        [product._id]: product,
                    },
                    result: product._id,
                },
            };

            return expectSaga(updateProduct, { payload: { productId, product }})
                .provide([
                    [matchers.call.fn(API.updateProduct), apiResponse],
                ])
                .put({
                    type: 'PRODUCTS_UPDATE_SUCCESS',
                    payload: {
                        response: apiResponse,
                    },
                })
                .dispatch('PRODUCTS_UPDATE_REQUEST')
                .run();
        })
    })

    describe ('removeProduct', () => {

        it ('dispatches productsRemoveSuccess', () => {

            const productId = '1';

            const apiResponse = {
                productId,
            };

            return expectSaga(removeProduct, { payload: { productId }})
                .provide([
                    [matchers.call.fn(API.removeProduct), apiResponse],
                ])
                .put({
                    type: 'PRODUCTS_REMOVE_SUCCESS',
                    payload: {
                        productId,
                    },
                })
                .dispatch('PRODUCTS_REMOVE_REQUEST')
                .run();
        })
    })
});
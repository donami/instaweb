import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import { Home } from '../client/containers/Home';

const mockStore = configureStore();

describe('test', () => {

    it ('should', () => {

        const props = {
            user: {
                username: 'fakeuser',
            },
            auth: {
                instagram: {
                    profile_picture: 'profile_picture_url',
                }
            },
        };

        const wrapper = shallow(<Home {...props} />);

        expect(1).toEqual(1);
    })
})
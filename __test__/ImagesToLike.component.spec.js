import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import ImagesToLike from '../client/components/ImagesToLike';

describe('ImagesToLike component', () => {

    let wrapper;

    const minProps = {
        pass: () => {},
        likeImage: () => {},
        order: {
            media: {
                images: {
                    thumbnail: {
                        url: 'image url',
                    },
                },
            },
        },
    };

    it ('should render', () => {

        wrapper = shallow(<ImagesToLike {...minProps} />);

        expect(wrapper.length).toEqual(1);
    });

    it ('should render a thumbnail image', () => {

        wrapper = shallow(<ImagesToLike {...minProps} />);

        expect(wrapper.find('img').prop('src')).toEqual(minProps.order.media.images.thumbnail.url);
    });

    it ('should contain Coins component', () => {

        wrapper = shallow(<ImagesToLike {...minProps} />);

        expect(wrapper.find('Coins').length).toEqual(1);
    });

    it ('should contain two buttons', () => {

        wrapper = shallow(<ImagesToLike {...minProps} />);

        expect(wrapper.find('Button').length).toEqual(2);
    });

    it ('should fire the likeImage action on button click', () => {

        let likeImageSpy = sinon.spy();

        wrapper = shallow(<ImagesToLike {...minProps} likeImage={likeImageSpy} />);

        wrapper.find('Button').at(0).simulate('click');

        expect(likeImageSpy.calledOnce).toEqual(true);
    });

    it ('should fire the pass action on button click', () => {

        let passSpy = sinon.spy();

        wrapper = shallow(<ImagesToLike {...minProps} pass={passSpy} />);

        wrapper.find('Button').at(1).simulate('click');

        expect(passSpy.calledOnce).toEqual(true);
    });
})
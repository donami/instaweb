import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import AdminProductsList from '../client/components/AdminProductsList';

describe ('AdminProductsList component', () => {

    let wrapper;

    const minProps = {
        products: [],
        remove: () => {},
    };

    const products = [
        { _id: '123bc123', name: 'fake product' },
    ];

    it ('should render', () => {

        wrapper = shallow(<AdminProductsList {...minProps} />);

        expect(wrapper.length).toEqual(1);
    });

    it ('should contain a List', () => {

        wrapper = shallow(<AdminProductsList {...minProps} />);

        expect(wrapper.find('List').length).toEqual(1);
    })

    it ('renders ListItem based on number of products', () => {

        wrapper = shallow(<AdminProductsList {...minProps} />);

        expect(wrapper.find('ListItem').length).toEqual(minProps.products.length);

        wrapper = shallow(<AdminProductsList {...minProps} products={products} />);

        expect(wrapper.find('ListItem').length).toEqual(products.length);
    })

    it ('executes remove action on button click', () => {

        const removeSpy = sinon.spy();

        wrapper = shallow(<AdminProductsList {...minProps} products={products} remove={removeSpy} />);

        wrapper.find('Button').simulate('click');

        expect(removeSpy.calledOnce).toEqual(true);
    })

})
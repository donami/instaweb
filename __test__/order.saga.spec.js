import { call, put, all } from 'redux-saga/effects';
import { expectSaga, testSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import API from '../client/services/api';
import * as instagramAPI from '../client/services/instagram';
import { loadOrders, likeImage, followUser } from '../client/sagas/order';

describe ('orderSaga', () => {


    describe ('loadOrders', () => {

        it('fetches the the orders', () => {

            return expectSaga(loadOrders, API)
                .provide([
                    [call(API.fetchOrders), { entities: {}, result: [] }],
                ])
                .put({
                    type: 'ORDERS_SUCCESS',
                    payload: {
                        response: { result: [], entities: {} },
                    },
                })
                .dispatch({ type: 'ORDERS_REQUEST' })
                .run();
        });

        it ('handles errors', () => {

            const error = new Error('error');

            return expectSaga(loadOrders, API)
                .provide([
                    [matchers.call.fn(API.fetchOrders), throwError(error)]
                ])
                .put({
                    type: 'ORDERS_FAILURE',
                    payload: error,
                    error: true,
                })
                .dispatch({ type: 'ORDERS_REQUEST' })
                .run();
        });
    });

    describe('likeImage', () => {

        it ('go through the flow', () => {

            const data = { payload: { reward: 5, userId: 1000 } };

            testSaga(likeImage, data)
                .next()
                .call(API.likeImage, data.payload)
                .next({ id: 1 })
                .all([
                    put({ type: 'LIKE_IMAGE_SUCCESS', payload: { order: { id: 1 } } }),
                    put({ type: 'RECEIVE_COINS', payload: { coins: data.payload.reward, userId: data.payload.userId } }),
                ])
                .next()
                .isDone();
        });
    });

    describe('followUser', () => {

        it ('go through the flow', () => {

            const order = {
                _id: 1,
                instagram: {
                    id: 2,
                },
            };
            const auth = { username: 'fakeuser', userId: 1, token: '1234cv.123.13' };
            const reward = 5;

            const data = {
                payload: {
                    reward,
                    order,
                    auth,
                },
            };

            testSaga(followUser, data)
                .next()
                .call(instagramAPI.getFollowing, auth.token)
                .next({ data: [] })
                .all([
                    call(instagramAPI.followUser, order.instagram.id, auth.token),
                    call(API.followUser, order._id, auth.userId)
                ])
                .next()
                .all([
                    put({
                        type: 'FOLLOW_USER_SUCCESS',
                        payload: {
                            instagramId: order.instagram.id
                        },
                    }),
                    put({
                        type: 'RECEIVE_COINS',
                        payload: {
                            coins: reward,
                            userId: auth.userId
                        },
                    })
                ])
                .next()
                .isDone();
        });
    });
});
import products from '../client/stores/products';

describe('products reducer', () => {

    let state;

    beforeEach(()=>{
        state = {
            ids: [],
            loaded: false,
            loading: false,
        };
    });

    describe ('PRODUCTS_CREATE_SUCCESS', () => {

        const product = {
            _id: '563c65b',
            name: 'fake product',
        };

        it ('should set add the id to ids', () => {
            state = products(state, {
                type: 'PRODUCTS_CREATE_SUCCESS',
                payload: {
                    response: {
                        result: product._id,
                        entities: {
                            products: {
                                [product._id]: product,
                            },
                        },
                    },
                },
            });

            expect(state.ids).toEqual([product._id]);
        });
    })

    describe ('PRODUCTS_REMOVE_SUCCESS', () => {

        const productId = 1;
        const initialState = {
            ids: [productId],
            loaded: false,
            loading: false,
        };

        it ('should remove the product id from ids', () => {

            state = products(initialState, {
                type: 'PRODUCTS_REMOVE_SUCCESS',
                payload: {
                    productId,
                },
            });

            expect(state.ids.indexOf(productId)).toEqual(-1);
        })
    })
})
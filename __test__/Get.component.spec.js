import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';

import { Get } from '../client/containers/Get';

describe('Get component', () => {

    let wrapper;

    const minProps = {
        auth: {
            _id: '5b12311c231cb1234',
            mediaLoaded: false,
            token: 'token',
        },
        userMediaRequest: () => {},
        createOrder: () => {},
        fetchProducts: () => {},
        productsLoaded: false,
        products: [],
    };

    it ('dispatches userMediaRequest and fetchProducts on mount', () => {

        const userMediaRequestSpy = sinon.spy();
        const fetchProductsSpy = sinon.spy();

        wrapper = mount(<Get {...minProps} userMediaRequest={userMediaRequestSpy} fetchProducts={fetchProductsSpy} />);

        expect(userMediaRequestSpy.calledOnce).toEqual(true);
        expect(fetchProductsSpy.calledOnce).toEqual(true);

    });

    it ('does not dispatch fetchProducts if already loaded', () => {

        const fetchProductsSpy = sinon.spy();

        wrapper = mount(<Get {...minProps} productsLoaded={true} fetchProducts={fetchProductsSpy} />);

        expect(fetchProductsSpy.notCalled).toEqual(true);
    })

})
FROM node:7.8.0

# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn

# Copy all local files into the image.
COPY . .

RUN npm install webpack serve -g

# Install all dependencies of the current project.
# COPY package.json package.json
RUN npm install

RUN npm run build

# Configure `serve`.
CMD serve -s dist
EXPOSE 5000

# Build for production.
# RUN npm run build --production